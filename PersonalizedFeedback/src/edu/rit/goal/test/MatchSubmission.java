package edu.rit.goal.test;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.rit.goal.ws.MatchingService;

public class MatchSubmission {
	public static void main(String[] args) throws Exception {
		NumberFormat nf = NumberFormat.getNumberInstance();
		((DecimalFormat) nf).applyPattern("00");
		
		int assignment = 1, solNumber = 2;
		String submissionFile = "assignments" + "/" + "running" + "/" + "sol" + nf.format(solNumber);
		
		Scanner scan = new Scanner(new File(submissionFile));
		StringBuffer submission = new StringBuffer();
		while (scan.hasNext()) {
			submission.append(scan.nextLine());
			submission.append("\n");
		}
		scan.close();
		
		String selectedPatterns = 
				"ACCESS_EVEN--" + 1 + "," + 
				"ACCESS_ODD--" + 1 + "," +
				"CUMULATIVELY_ADD--" + 1 + "," +
				"CUMULATIVELY_MULT--" + 1 + "," +
				"PRINT_CONSOLE--" + 2 + ",";
		selectedPatterns = selectedPatterns.substring(0, selectedPatterns.length() - 1);
		
		String constraints = 
				"ACCESS_EVEN.6=CUMULATIVELY_MULT.2|Variable :c is [not] accessing even positions in :s," +
				"ACCESS_ODD.6=CUMULATIVELY_ADD.2|Variable :c is [not] accessing odd positions in :s," +
				"PRINT_CONSOLE.1=CUMULATIVELY_ADD.2|Variable :x is [not] cumulatively added and printed to console," +
				"PRINT_CONSOLE.1=CUMULATIVELY_MULT.2|Variable :x is [not] cumulatively multiplied and printed to console,";
		constraints = constraints.substring(0, constraints.length() - 1);
		
		MatchingService service = new MatchingService();
		String result = service.match(assignment, submission.toString(), selectedPatterns, constraints, Integer.MAX_VALUE);
		
		JSONObject obj = new JSONObject(result);
		JSONArray feedback = obj.getJSONArray("feedback");
		
		for (int i = 0; i < feedback.length(); i++)
			System.out.println(feedback.getJSONObject(i));
	}
}
