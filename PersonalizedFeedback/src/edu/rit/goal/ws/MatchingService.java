package edu.rit.goal.ws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import edu.rit.goal.epdg.EPDGParser;
import edu.rit.goal.epdg.object.Pattern;
import edu.rit.goal.epdg.object.Pattern.PatternType;
import edu.rit.goal.epdg.object.PatternGraph;
import edu.rit.goal.epdg.object.Solution;

@Path("/pf")
public class MatchingService {
	@GET
	@Path("/match")
	public Response match(
			@QueryParam("assignment") int assignment,
			@QueryParam("submission") String submission,
			@QueryParam("patterns") String selectedPatterns) {
		return Response.ok(match(assignment, submission , selectedPatterns, null, 1000))
					.header("Access-Control-Allow-Origin", "*")
					.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
					.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}
	
	public String match(int assignment, String submission, String selectedPatterns, String constraints, int limit) {
		// Let's parse the submission.
		EPDGParser parser = new EPDGParser();
		
		// First, we need to wrap it with a class and a method.
		String wrapped = wrapSubmission(assignment, submission);
		
		// Then, we actually parse it.
		String result = null;
		List<String> errors = new ArrayList<>();
		
		// We will not allow more than 'limit' chars.
		if (wrapped.length() <= limit) {
			try {
				parser.parseJavaClass(wrapped, 2);
			} catch (Exception oops) {
				errors.add("when parsing the submission");
			}
			
			// Store the solutions to check the constraints.
			Map<PatternType, List<Solution>> solutionsByPatternType = new HashMap<>();
			
			if (errors.isEmpty() && parser.getErrorsJSON().isEmpty())
				try {
					String[] selected = selectedPatterns.split(",");
					
					for (String currentPattern : selected) {
						String[] typeAndNumber = currentPattern.split("--");
						
						PatternType type = PatternType.valueOf(typeAndNumber[0]);
						int times = Integer.valueOf(typeAndNumber[1]);
						try {
							parser.matchGraph(Pattern.getInstance().getPattern(type), times);
							solutionsByPatternType.put(type, new ArrayList<>(parser.getSolutions()));
						} catch (Exception oops) {
							errors.add("Something went wrong when matching pattern " + Pattern.getInstance().getPattern(type).getDescription() + ".");
						}
					}
				} catch (Exception oops) {
					errors.add("Something went wrong when parsing the selected patterns.");
				}
			
			
			if (errors.isEmpty() && parser.getErrorsJSON().isEmpty())
				try {
					String[] constraintArray = constraints.split(",");
					
					for (String constraint : constraintArray) {
						java.util.regex.Pattern p = java.util.regex.Pattern.compile("([A-Z0-9\\_]+)\\.([0-9]+)=([A-Z0-9\\_]+)\\.([0-9]+)\\|(.+)");
						Matcher m = p.matcher(constraint);
						
						if (m.find()) {
							PatternType type1 = PatternType.valueOf(m.group(1));
							PatternGraph pattern1 = Pattern.getInstance().getPattern(type1);
							Integer node1 = Integer.valueOf(m.group(2));
							
							PatternType type2 = PatternType.valueOf(m.group(3));
							PatternGraph pattern2 = Pattern.getInstance().getPattern(type2);
							Integer node2 = Integer.valueOf(m.group(4));
							
							String feedback = m.group(5);
							
							parser.matchConstraint(pattern1, node1, solutionsByPatternType.get(type1), pattern2, node2, solutionsByPatternType.get(type2), feedback);
						} else
							throw new Exception("Something went wrong when parsing the constraints.");
					}
				} catch (Exception oops) {
					errors.add("Something went wrong when parsing the constraints.");
				}
		}
		else
			errors.add("Please, no more than " + limit + " characters.");
		
		if (errors.isEmpty() && parser.getErrorsJSON().isEmpty())
			result = parser.getResultJSON();
		
		if (!parser.getErrorsJSON().isEmpty())
			result = parser.getErrorsJSON();
		if (!errors.isEmpty()) {
			StringBuilder errorBuilder = new StringBuilder("{\"error\" : [");
			for (String err : errors) {
				errorBuilder.append("\"");
				errorBuilder.append(err);
				errorBuilder.append("\",");
			}
			errorBuilder.deleteCharAt(errorBuilder.length() - 1);
			errorBuilder.append("]}");
			result = errorBuilder.toString();
		}
		
		return result;
	}
	
	private String wrapSubmission(int assignment, String submission) {
		StringBuilder sb = new StringBuilder("public class Submission {\n");
		sb.append("public void ");
		sb.append(getFunctionName(assignment));
		
		if (assignment == 1)
			sb.append("(int[] a)");
		else if (assignment == 2)
			sb.append("(String f, int year)");
				
		sb.append(" {\n");
		sb.append(submission);
		sb.append("}}");
		
		return sb.toString();
	}
	
	private String getFunctionName(int assignment) {
		return "assignment" + assignment;
	}
	
}