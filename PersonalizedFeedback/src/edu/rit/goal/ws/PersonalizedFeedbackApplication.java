package edu.rit.goal.ws;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/feedback")
public class PersonalizedFeedbackApplication extends Application { }