package edu.rit.goal.epdg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import edu.rit.goal.epdg.object.Edge;
import edu.rit.goal.epdg.object.EdgeType;
import edu.rit.goal.epdg.object.Feedback;
import edu.rit.goal.epdg.object.Feedback.FeedbackType;
import edu.rit.goal.epdg.object.PatternGraph;
import edu.rit.goal.epdg.object.PatternVertex;
import edu.rit.goal.epdg.object.Solution;
import edu.rit.goal.epdg.object.Vertex;
import edu.rit.goal.epdg.object.VertexType;

public class EPDGParser {
	private Java8BaseListener listener;
	private DirectedGraph<Vertex, Edge> graph = null;
	private Map<VertexType, List<Vertex>> verticesByType = new HashMap<>();
	private Map<Feedback, List<Feedback>> feedback = new HashMap<>();
	
	public void parseJavaClass(String body, int errorLineOffset) {
		Lexer lexer = new Java8Lexer(new ANTLRInputStream(body));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		Java8Parser parser = new Java8Parser(tokens);
		
		listener = new Java8BaseListener(errorLineOffset);
		
		parser.removeErrorListeners();
		parser.addErrorListener(new ErrorListener(listener));

		ParserRuleContext t = parser.compilationUnit();
		ParseTreeWalker walker = new ParseTreeWalker();
		walker.walk(listener, t);
		
		// Generate the graph.
		graph = new DefaultDirectedGraph<>(Edge.class);
		for (Vertex current : listener.getVertices()) {
			graph.addVertex(current);
			
			List<Vertex> list = verticesByType.get(current.getType());
			if (list == null)
				verticesByType.put(current.getType(), list = new ArrayList<>());
			list.add(current);
		}
		for (Edge current : listener.getEdges())
			graph.addEdge(current.getFromVertex(), current.getToVertex(), current);
	}
	
	public String getResultJSON() {
		StringBuilder jSON = new StringBuilder("{\"graph\" : [");
		
		for (Vertex current : listener.getVertices())
			jSON.append("{\"data\": { "
					+ "\"id\": " + current.getId() + ", "
					+ "\"label\": \"v" + current.getId() + ", " + current.getType() + "\", " 
					+ "\"code\": \"" + current.getLabel().replaceAll("\"", "\\\\\"") + "\" }},");
		for (Edge current : listener.getEdges()) {
			jSON.append("{\"data\": { "
					+ "\"id\": \"" + current.getFromVertex().getId() + "-->" + current.getToVertex().getId() + "\""
					+ ", \"source\": " + current.getFromVertex().getId() + ""
					+ ", \"target\": " + current.getToVertex().getId() + "}");
			if (current.getType().equals(EdgeType.CTRL))
				jSON.append(", \"classes\": \"control\"");
			jSON.append("},");
		}
		jSON.deleteCharAt(jSON.length() - 1);
		jSON.append("], \"feedback\" : [");
		
		for (Feedback f : feedback.keySet()) {
			jSON.append("{\"type\": \"" + f.getType() + "\", " + "\"text\": \"" + f.getText() + "\", \"minor\": ");
			jSON.append("[");
			for (Feedback minor : feedback.get(f))
				jSON.append("{\"type\": \"" + minor.getType() + "\", " + "\"text\": \"" + minor.getText() + "\"},");
			if (!feedback.get(f).isEmpty())
				jSON.deleteCharAt(jSON.length() - 1);
			jSON.append("]},");
		}
		if (!feedback.isEmpty())
			jSON.deleteCharAt(jSON.length() - 1);
		
		jSON.append("]}");
		
		return jSON.toString();
	}
	
	public String getErrorsJSON() {
		StringBuilder error = new StringBuilder();
		
		if (!listener.getErrorMessages().isEmpty()) {
			error.append("{\"error\" : [");
			for (String err : listener.getErrorMessages()) {
				error.append("\"");
				error.append(err);
				error.append("\",");
			}
			error.deleteCharAt(error.length() - 1);
			error.append("]}");
		}
		
		return error.toString();
	}
	
	private List<Solution> solutions = new ArrayList<>();
	
	private boolean checkEdge(Edge pe, Vertex matchingFrom, Vertex matchingTo) {
		boolean check = true;
		if (pe != null)
			// Check if the edge exists.
			check = graph.containsEdge(matchingFrom, matchingTo);
		return check;
	}
	
	private String substitute(String str, Solution sol, String pre, String post) {
		for (String uVar : sol.getVarMapping().keySet())
			str = str.replaceAll(uVar, pre + sol.getVarMapping().get(uVar) + post);
		return str;
	}
	
	private void computeSolutions(PatternGraph pattern, Map<PatternVertex, List<Vertex>> searchSpace, int depth, Solution current) {
		if (depth == searchSpace.size())
			solutions.add(current.clone());
		else {
			// Pick a random node not yet processed.
			PatternVertex u = Sets.difference(searchSpace.keySet(), current.getNodeMapping().keySet()).iterator().next();
			
			for (Vertex v : searchSpace.get(u)) {
				List<Map<String, String>> listOfVarMappings = new ArrayList<>();
				
				// No duplicates.
				boolean check = !current.getNodeMapping().containsValue(v);
				for (Iterator<PatternVertex> nodesInCurrentIt = current.getNodeMapping().keySet().iterator(); check && nodesInCurrentIt.hasNext(); ) {
					PatternVertex otherU = nodesInCurrentIt.next();
					
					check = check && checkEdge(pattern.getGraph().getEdge(u, u), v, v) && 
							checkEdge(pattern.getGraph().getEdge(u, otherU), v, current.getNodeMapping().get(otherU)) &&
							checkEdge(pattern.getGraph().getEdge(otherU, u), current.getNodeMapping().get(otherU), v);
				}
				
				if (check) {
					Map<String, String> varMappings = new HashMap<>();
					Set<String> pendingUVars = new HashSet<>(u.getReadingVariables());
					Set<String> pendingVVars = new HashSet<>(v.getReadingVariables());
					
					pendingUVars.removeAll(current.getVarMapping().keySet());
					pendingVVars.removeAll(current.getVarMapping().values());
					
					if (u.getAssignedVariable() != null && !current.getVarMapping().containsKey(u.getAssignedVariable()) && 
							v.getAssignedVariable() != null && !current.getVarMapping().containsValue(v.getAssignedVariable())) {
						varMappings.put(u.getAssignedVariable(), v.getAssignedVariable());
						
						// Already checked.
						pendingUVars.remove(u.getAssignedVariable());
						pendingVVars.remove(v.getAssignedVariable());
					}
					
					// Compute all permutations.
					List<String> listOfPending = new ArrayList<>(pendingUVars);
					List<Set<String>> allPermutations = new ArrayList<>();
					for (int i = 0; i < listOfPending.size(); i++)
						allPermutations.add(new HashSet<>(pendingVVars));
					
					for (List<String> permutation: Sets.cartesianProduct(allPermutations)) {
						Map<String, String> newVarMappings = new HashMap<>(varMappings);
						for (int i = 0; i < permutation.size(); i++)
							newVarMappings.put(listOfPending.get(i), permutation.get(i));
						listOfVarMappings.add(newVarMappings);
					}
				}
				
				if (check)
					for (Map<String, String> newVarMapping : listOfVarMappings) {
						current.getVarMapping().putAll(newVarMapping);
						
						String uLabel = substitute(u.getLabel(), current, "", "");
						
						// Check perfect matching.
						boolean matching = v.getLabel().contains(uLabel);
						boolean removeApprox = false;
						
						// Let's try approximate matching
						if (!matching && u.getApproxLabel() != null) {
							String uApproxLabel = substitute(u.getApproxLabel(), current, "", "");
							matching = v.getLabel().matches(uApproxLabel);

							if (matching) {
								current.getApproxNodes().add(u);
								removeApprox = true;
							}
						}
						
						if (matching) {
							current.getNodeMapping().put(u, v);
							computeSolutions(pattern, searchSpace, depth + 1, current);
							current.getNodeMapping().remove(u);
						}
						
						if (removeApprox)
							current.getApproxNodes().remove(u);
						
						for (String uVar : newVarMapping.keySet())
							current.getVarMapping().remove(uVar);
					}
			}
		}
	}
	
	private void computeSolutions(PatternGraph pattern) {
		solutions.clear();
		
		// Compute search space.
		Map<PatternVertex, List<Vertex>> searchSpace = new HashMap<>();
		boolean emptySearchSpace = false;
		for (PatternVertex v : pattern.getGraph().vertexSet()) {
			List<Vertex> matchingVertices = verticesByType.get(v.getType());
			if (matchingVertices != null)
				searchSpace.put(v, matchingVertices);
			else
				emptySearchSpace = true;
		}
		
		if (!emptySearchSpace)
			// Compute solutions.
			computeSolutions(pattern, searchSpace, 0, new Solution());
	}
	
	private void generateDetailedFeedBack(Solution solution, PatternGraph pattern, Map<Feedback, List<Feedback>> feedback) {
		Feedback main = null;
		if (!solution.getApproxNodes().isEmpty())
			main = new Feedback(FeedbackType.Almost, "You almost got " + pattern.getDescription() + ".");
		else
			main = new Feedback(FeedbackType.Correct, "You correctly got " + pattern.getDescription() + ".");
		List<Feedback> detailed = new ArrayList<>();
		feedback.put(main, detailed);
			
		for (PatternVertex u : solution.getNodeMapping().keySet())
			if (solution.getApproxNodes().contains(u) && u.getIncorrectFeedback() != null)
				detailed.add(new Feedback(FeedbackType.Incorrect, substitute(u.getIncorrectFeedback(), solution, "<b>", "</b>")));
			else if (!solution.getApproxNodes().contains(u) && u.getCorrectFeedback() != null)
				detailed.add(new Feedback(FeedbackType.Correct, substitute(u.getCorrectFeedback(), solution, "<b>", "</b>")));
	}
	
	private Map<Feedback, List<Feedback>> getFeedback(PatternGraph pattern, int times) {
		Map<Feedback, List<Feedback>> feedback = new HashMap<>();
		if (solutions.size() != times)
			feedback.put(new Feedback(FeedbackType.Incorrect, "You incorrectly got " + pattern.getDescription() + "."), 
					Lists.newArrayList(
							new Feedback(FeedbackType.Incorrect, 
									"We found you were performing this task " + solutions.size() + " time" + 
										(solutions.size() != 1 ? "s" : "") + ", we were expecting to find " + times + "."),
							new Feedback(FeedbackType.Incorrect, 
									"Please, " + pattern.getMissingFeedback() + ".")));
		else if (!solutions.isEmpty())
			// Generate detailed feedback for those that have matched, e.g., the range is not correct.
			for (Solution current : solutions)
				generateDetailedFeedBack(current, pattern, feedback);
		else
			feedback.put(new Feedback(FeedbackType.Incorrect, "You incorrectly got " + pattern.getDescription() + "."), 
					Lists.newArrayList(
							new Feedback(FeedbackType.Incorrect, 
									"We did not found that you are performing this task."),
							new Feedback(FeedbackType.Incorrect, 
									"Please, " + pattern.getMissingFeedback() + ".")));
		
		return feedback;
	}
	
	public void matchGraph(PatternGraph pattern, int times) {
		if (times > 0) {
			computeSolutions(pattern);
			feedback.putAll(getFeedback(pattern, times));
		}
	}
	
	public void matchConstraint(PatternGraph pattern1, Integer node1, List<Solution> solutions1, PatternGraph pattern2, Integer node2, List<Solution> solutions2, String feedbackStr) {
		boolean found = false;
		
		// Get the solutions for type 1.
		for (Iterator<Solution> sol1It = solutions1.iterator(); !found && sol1It.hasNext(); ) {
			Solution sol1 = sol1It.next();
			Vertex v1 = sol1.getNodeMapping().get(pattern1.findNodeById(node1));
			
			// Get the solutions for type 2.
			for (Iterator<Solution> sol2It = solutions2.iterator(); !found && sol2It.hasNext(); ) {
				Solution sol2 = sol2It.next();
				Vertex v2 = sol2.getNodeMapping().get(pattern2.findNodeById(node2));
				
				if (v1.equals(v2)) {
					found = true;
					feedbackStr = feedbackStr.replace(" [not]", "");
					feedbackStr = substitute(feedbackStr, sol1, "<b>", "</b>");
					feedbackStr = substitute(feedbackStr, sol2, "<b>", "</b>");
					
					feedback.put(new Feedback(FeedbackType.Correct, feedbackStr + "."), new ArrayList<>());
				}
			}	
		}
		
		if (!found && !solutions1.isEmpty() && !solutions2.isEmpty()) {
			feedbackStr = feedbackStr.replace("[", "").replace("]", "");
			feedbackStr = feedbackStr.replaceAll(" \\:[a-z]", "");
			
			feedback.put(new Feedback(FeedbackType.Incorrect, feedbackStr + "."), new ArrayList<>());
		}
	}

	public List<Solution> getSolutions() {
		return solutions;
	}
	
}