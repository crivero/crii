package edu.rit.goal.epdg.object;

import java.util.Arrays;

public class StrictVertex {
	private Vertex vertex;
	private String[] hardValues;
	
	public StrictVertex(Vertex newVertex, String[] hardValues) {
		vertex = newVertex;
		this.hardValues = hardValues;
	}
	
	public void setVertex(Vertex newVertex) {
		vertex = newVertex;
	}
	
	public void sethardValues(String[] _hardValues) {
		hardValues = _hardValues;
	}
	
	public String[] getHardValues() {
		return hardValues;
	}
	
	public Vertex getVertex() {
		return vertex;
	}

	@Override
	public String toString() {
		return vertex + ", Hard values: " + Arrays.toString(hardValues);
	}
}
