package edu.rit.goal.epdg.object;

import java.util.HashSet;
import java.util.Set;

public class Pattern {
	private static Pattern instance = null;
	private PatternGraph patternAccessingOddPositions = new PatternGraph("Accessing odd positions in an array", "consider using a loop and a condition, recall that odd is computed by % 2 == 1");
	private PatternGraph patternAccessingEvenPositions = new PatternGraph("Accessing even positions in an array", "consider using a loop and a condition, recall that even is computed by % 2 == 0");
	private PatternGraph patternConditionalCumulativeAdding = new PatternGraph("Conditional cumulatively adding", "consider using a loop and a condition, recall to add and assign the same variable");
	private PatternGraph patternConditionalCumulativeMultiplication = new PatternGraph("Conditional cumulatively multiplying", "consider using a loop and a condition, recall to multiply and assign the same variable");
	private PatternGraph patternAssignAndPrintingConsole = new PatternGraph("Assign and print to console", "consider working with a variable and printing it to console using System.out.print");
	
	private Pattern() {
		createAccessingOddPositions(patternAccessingOddPositions);
		createAccessingEvenPositions(patternAccessingEvenPositions);
		createConditionalCumulativeAdding(patternConditionalCumulativeAdding);
		createConditionalCumulativeMultiplication(patternConditionalCumulativeMultiplication);
		createAssigneAndPrintToConsole(patternAssignAndPrintingConsole);
	}
	
	private void createAccessingOddPositions(PatternGraph pattern) {
		PatternVertex one = new PatternVertex(1);
		one.setType(VertexType.DECL);
		one.setLabel(":s");
		Set<String> s = new HashSet<String>();
		s.add(":s");
		one.getReadingVariables().addAll(s);
		
		PatternVertex two = new PatternVertex(2);
		two.setAssignedVariable(":x");
		two.setType(VertexType.ASSIGN);
		two.setLabel(":x=0");
		two.setApproxLabel(".*:x.*");
		two.setCorrectFeedback(":x starts in 0");
		two.setIncorrectFeedback(":x should start in 0");
		s.clear();
		s.add(":x");
		two.getReadingVariables().addAll(s);
		
		PatternVertex three = new PatternVertex(3);
		three.setAssignedVariable(":x");
		three.setType(VertexType.ASSIGN);
		three.setLabel(":x++");
		three.setApproxLabel(":x.*");
		three.setCorrectFeedback(":x is incremented in 1");
		three.setIncorrectFeedback(":x should be incremented in 1");
		s.clear();
		s.add(":x");
		three.getReadingVariables().addAll(s);
		
		PatternVertex four = new PatternVertex(4);
		four.setType(VertexType.CTRL);
		four.setLabel(":x<:s.length");
		four.setApproxLabel(":x<=:s.length");
		four.setCorrectFeedback(":x does not go beyond :s.length - 1");
		four.setIncorrectFeedback(":x should not go beyond :s.length - 1");
		s.clear();
		s.add(":s");
		s.add(":x");
		four.getReadingVariables().addAll(s);
		
		PatternVertex five = new PatternVertex(5);
		five.setType(VertexType.CTRL);
		five.setLabel(":x%2==1");
		five.setCorrectFeedback("You are using % 2 == 1 to control :x is odd");
		s.clear();
		s.add(":x");
		five.getReadingVariables().addAll(s);
		
		PatternVertex six = new PatternVertex(6);
		six.setType(VertexType.ASSIGN);
		six.setLabel(":s[:x]");
		six.setApproxLabel(".*:s\\[.*:x.*\\].*");
		six.setCorrectFeedback(":x is used exactly to access :s");
		six.setIncorrectFeedback(":x should be used exactly to access :s");
		s.clear();
		s.add(":x");
		s.add(":s");
		six.getReadingVariables().addAll(s);
		
		pattern.getGraph().addVertex(one);
		pattern.getGraph().addVertex(two);
		pattern.getGraph().addVertex(three);
		pattern.getGraph().addVertex(four);
		pattern.getGraph().addVertex(five);
		pattern.getGraph().addVertex(six);
		
		pattern.getGraph().addEdge(one, four, new Edge(one, four, EdgeType.DATA));
		pattern.getGraph().addEdge(one, six, new Edge(one, six, EdgeType.DATA));
		pattern.getGraph().addEdge(two, three, new Edge(two, three, EdgeType.DATA));
		pattern.getGraph().addEdge(two, four, new Edge(two, four, EdgeType.DATA));
		pattern.getGraph().addEdge(two, five, new Edge(two, five, EdgeType.DATA));
		pattern.getGraph().addEdge(two, six, new Edge(two, six, EdgeType.DATA));
		pattern.getGraph().addEdge(four, three, new Edge(four, three, EdgeType.CTRL));
		pattern.getGraph().addEdge(four, five, new Edge(four, five, EdgeType.CTRL));
		pattern.getGraph().addEdge(five, six, new Edge(five, six, EdgeType.CTRL));
	}
	
	private PatternGraph createAccessingEvenPositions(PatternGraph pattern) {
		createAccessingOddPositions(pattern);
		
		for (PatternVertex u : pattern.getGraph().vertexSet())
			if (u.getLabel().equals(":x%2==1")) {
				u.setLabel(":x%2==0");
				u.setCorrectFeedback("You are using % 2 == 0 to control :x is even");
			}
		
		return pattern;
	}
	
	private void createConditionalCumulativeAdding(PatternGraph pattern) {
		PatternVertex one = new PatternVertex(1);
		one.setAssignedVariable(":c");
		one.setType(VertexType.ASSIGN);
		one.setLabel(":c=0");
		one.setApproxLabel(":c=\\d+");
		one.setCorrectFeedback(":c is initialized to 0");
		one.setIncorrectFeedback(":c should be initialized to 0");
		Set<String> s = new HashSet<String>();
		s.add(":c");
		one.getReadingVariables().addAll(s);
		
		PatternVertex two = new PatternVertex(2);
		two.setAssignedVariable(":c");
		two.setType(VertexType.ASSIGN);
		two.setLabel(":c+=");
		two.setCorrectFeedback(":c is cumulatively added");
		s.clear();
		s.add(":c");
		two.getReadingVariables().addAll(s);
		
		PatternVertex three = new PatternVertex(3);
		three.setType(VertexType.CTRL);
		three.setLabel("");
		three.setCorrectFeedback(":c is in a condition");
		s.clear();
		three.getReadingVariables().addAll(s);
		
		PatternVertex four = new PatternVertex(4);
		four.setType(VertexType.CTRL);
		four.setLabel("");
		four.setCorrectFeedback(":c is in a loop");
		s.clear();
		four.getReadingVariables().addAll(s);
		
		pattern.getGraph().addVertex(one);
		pattern.getGraph().addVertex(two);
		pattern.getGraph().addVertex(three);
		pattern.getGraph().addVertex(four);
		
		pattern.getGraph().addEdge(one, two, new Edge(one, two, EdgeType.DATA));
		pattern.getGraph().addEdge(three, two, new Edge(three, two, EdgeType.CTRL));
		pattern.getGraph().addEdge(three, two, new Edge(three, two, EdgeType.CTRL));
		pattern.getGraph().addEdge(four, three, new Edge(four, three, EdgeType.CTRL));
		pattern.getGraph().addEdge(four, four, new Edge(four, four, EdgeType.CTRL));
	}
	
	private void createConditionalCumulativeMultiplication(PatternGraph pattern) {
		createConditionalCumulativeAdding(pattern);
		
		for (PatternVertex u : pattern.getGraph().vertexSet())
			if (u.getLabel().equals(":c=0")) {
				u.setLabel(":c=1");
				u.setCorrectFeedback(":c is initialized to 1");
				u.setIncorrectFeedback(":c should be initialized to 1");
			} else if (u.getLabel().equals(":c+=")) {
				u.setLabel(":c*=");
				u.setCorrectFeedback(":c is cumulatively multiplied");
			}
	}
	
	private void createAssigneAndPrintToConsole(PatternGraph pattern) {
		PatternVertex one = new PatternVertex(1);
		one.setAssignedVariable(":x");
		one.setType(VertexType.ASSIGN);
		one.setLabel(":x");
		one.setCorrectFeedback(":x is assigned");
		one.setIncorrectFeedback(":x is not assigned");
		Set<String> s = new HashSet<String>();
		s.clear();
		s.add(":x");
		one.getReadingVariables().addAll(s);
		
		PatternVertex two = new PatternVertex(2);
		two.setType(VertexType.CALL);
		two.setLabel("System.out.print");
		two.setCorrectFeedback(":x is printed to console");
		two.setIncorrectFeedback(":x is not printed to console");
		s.clear();
		s.add(":x");
		two.getReadingVariables().addAll(s);
		
		pattern.getGraph().addVertex(one);
		pattern.getGraph().addVertex(two);
		
		pattern.getGraph().addEdge(one, two, new Edge(one, two, EdgeType.DATA));
	}
	
	public static Pattern getInstance() {
		if(instance == null)
			instance = new Pattern();
		return instance;
	}
	
	public PatternGraph getPattern(PatternType type) {
		PatternGraph pattern = null;
		if (type.equals(PatternType.ACCESS_ODD))
			pattern = patternAccessingOddPositions;
		if (type.equals(PatternType.ACCESS_EVEN))
			pattern = patternAccessingEvenPositions;
		if (type.equals(PatternType.CUMULATIVELY_ADD))
			pattern = patternConditionalCumulativeAdding;
		if (type.equals(PatternType.CUMULATIVELY_MULT))
			pattern = patternConditionalCumulativeMultiplication;
		if (type.equals(PatternType.PRINT_CONSOLE))
			pattern = patternAssignAndPrintingConsole;
		return pattern;
	}
	
	public enum PatternType {
		ACCESS_ODD, ACCESS_EVEN, CUMULATIVELY_ADD, CUMULATIVELY_MULT, PRINT_CONSOLE
	}
	
}