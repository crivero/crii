package edu.rit.goal.epdg.object;

public class Feedback {
	private FeedbackType type;
	private String text;

	public Feedback(FeedbackType type, String text) {
		super();
		this.type = type;
		this.text = text;
	}

	public FeedbackType getType() {
		return type;
	}

	public String getText() {
		return text;
	}
	
	public enum FeedbackType {
		Correct, Incorrect, Almost
	}
	
}