package edu.rit.goal.epdg.object;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class Vertex {
	private int id;
	private VertexType type;
	private String label, assignedVariable;
	private Set<String> readingVariables;

	public Vertex(int id) {
		this.id = id;
		readingVariables = new HashSet<>();
	}
	
	public int getId() {
		return id;
	}
	
	public String getAssignedVariable() {
		return assignedVariable;
	}

	public void setAssignedVariable(String assignedVariable) {
		this.assignedVariable = assignedVariable;
	}

	public VertexType getType() {
		return type;
	}

	public void setType(VertexType type) {
		this.type = type;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Set<String> getReadingVariables() {
		return readingVariables;
	}

	@Override
	public String toString() {
		return id + "-" + type + "-" + label;
	}
	public String getHardCode() {
		String hardCode = label.replaceAll("[a-zA-Z]", "");
		hardCode = hardCode.replaceAll(" ", "");
		return hardCode;
	}
	public LinkedHashSet<String> getVarSet() {
		LinkedHashSet<String> variables = new LinkedHashSet<String>();
		String labelCopy = new String(label);
		String alphaOnly = labelCopy.replaceAll("[^\\p{Alpha}]+",",");
	    alphaOnly = alphaOnly.replaceAll("length", "");
	    alphaOnly = alphaOnly.replaceAll(",,", ",");
	    alphaOnly = alphaOnly.replaceAll(" ", "");
	    String[] vars = alphaOnly.split(",");
	    for(String var: vars) {
	    	if(readingVariables.contains(var)) {
	    		variables.add(var);
	    	}
	    }
		return variables;
	}
	
}
