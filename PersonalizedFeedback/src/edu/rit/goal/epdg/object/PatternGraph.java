package edu.rit.goal.epdg.object;

import java.util.Iterator;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;

public class PatternGraph {
	private DirectedGraph<PatternVertex, Edge> graph = new DefaultDirectedGraph<>(Edge.class);
	private String description;
	private String missingFeedback;
	
	public PatternGraph(String description, String missingFeedback) {
		this.description = description;
		this.missingFeedback = missingFeedback;
	}
	
	public DirectedGraph<PatternVertex, Edge> getGraph() {
		return graph;
	}

	public String getDescription() {
		return "<strong>" + description + "</strong>";
	}

	public String getMissingFeedback() {
		return missingFeedback;
	}
	
	public PatternVertex findNodeById(int id) {
		PatternVertex ret = null;
		for (Iterator<PatternVertex> it = graph.vertexSet().iterator(); ret == null && it.hasNext(); ) {
			PatternVertex current = it.next();
			if (current.getId() == id)
				ret = current;
		}
		return ret;
	}
	
}
