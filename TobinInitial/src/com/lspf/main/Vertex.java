package com.lspf.main;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Vertex implements Comparable<Vertex>, Serializable {
	private String label;
	private String type;

	public Vertex(Vertex vertexToCopy) {
		this.label = vertexToCopy.getLabel();
		this.type = vertexToCopy.getType();

	}

	public Vertex(String label, String type) {
		this.label = label;
		this.type = "%" + type + "%";
	}

	public String getType() {

		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int compareTo(Vertex o) {
		return (this.getLabel().compareTo(o.getLabel()));
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;

	}

	@Override
	public String toString() {
		if (type.equals("assign")) {
			return (label);
		} else if (type.equals("decl")) {
			return (label);
		} else {
			return label;
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}