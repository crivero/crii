package com.lspf.main;

// Generated from Java8.g4 by ANTLR 4.5.1

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Stack;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 * This class provides an empty implementation of {@link Java8Listener}, which
 * can be extended to create a listener which only needs to handle a subset of
 * the available methods.
 */

@SuppressWarnings("serial")
public class Java8BaseListener implements Java8Listener, Serializable {

	private Vertex currentVertex = null;

	private boolean logger = false;
	public static int countCount = 0;

	public static int for_count = 0;
	public static int while_count = 0;
	public static int if_count = 0;
	private static Stack<String> controlAndCountStack = new Stack<String>();

	public static boolean var_for = false;
	public static boolean var_while = false;
	public static boolean var_if = false;

	public static String function_name = "";
	public static Stack<String> method = new Stack<String>();
	public static ArrayList<Edge> edgeList = new ArrayList<Edge>();

	public static ArrayList<Vertex> declList = new ArrayList<Vertex>();
	public static ArrayList<Vertex> returnList = new ArrayList<Vertex>();
	public static ArrayList<Vertex> assignList = new ArrayList<Vertex>();
	public static ArrayList<Vertex> controlList = new ArrayList<Vertex>();
	public static ArrayList<Vertex> exprList = new ArrayList<Vertex>();
	public static ArrayList<Vertex> incList = new ArrayList<Vertex>();
	public static ArrayList<Vertex> callSiteList = new ArrayList<Vertex>();
	public static Stack<Vertex> controlstack = new Stack<Vertex>();
	public static HashMap<String, String> type = new HashMap<String, String>();
	public static HashMap<String, HashMap<String, Vertex>> lastAssignmentVertex = new HashMap<String, HashMap<String, Vertex>>();
	public static HashMap<String, Vertex> functionVertex = new HashMap<String, Vertex>();
	// boolean enterforStructure =false;
	boolean ForUpdate = false;
	boolean enterforStructure = false;
	static boolean enterFor = false;
	static boolean enterWhile = false;
	static boolean enterIf = false;
	boolean enterMethod = false;
	boolean equality = false;
	boolean assignment = false;
	boolean for_update = false;

	Vertex expression = null;

	private void printStuff() {
		if (method.isEmpty())
			return;
		if ((method.peek().toString().equals(function_name))) {
			System.out.println("FN" + function_name);
			System.out.println("DL" + declList);
			System.out.println("Assign List" + assignList);
			System.out.println("control list" + controlList);
			System.out.println("control stack" + controlstack);
			System.out.println("Expression List" + exprList);
			System.out.println("incList" + incList);
			System.out.println("edge list" + edgeList);
			System.out.println("last ass:" + lastAssignment);
			System.out.println();
			System.out.println();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterExpressionName(Java8Parser.ExpressionNameContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("EXPRESSION NAME LINE 62:");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			if (enterMethod) {
				enterMethod = false;
			}
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}

	}

	private void createEdge(Vertex one, Vertex two, String type) {
		Edge assign_assign = new Edge(one, two, type);
		edgeList.add(assign_assign);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterVariableDeclarator(
			Java8Parser.VariableDeclaratorContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("VARIABLE DECLARATOR LINE 114");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			String current = ctx.getText();
			Vertex assignNode = new Vertex(current, "assign");
			currentVertex = assignNode;
			addToAssignment(assignNode);
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	private void addToLastAssignmentVertex(Vertex current) {
		String label = current.getLabel();
		if (label.contains("$function")) {
			label = label.substring(label.indexOf("$function"),
					label.indexOf("()"));
			HashMap<String, Vertex> cloneOfLastAssign = new HashMap<String, Vertex>();
			Set<String> keys = lastAssignment.keySet();
			for (String key : keys) {
				cloneOfLastAssign.put(key, lastAssignment.get(key));
			}
			lastAssignmentVertex.put(label, cloneOfLastAssign);
			functionVertex.put(label, current);
		}
	}

	public static HashMap<String, Vertex> lastAssignment = new HashMap<String, Vertex>();

	private void addToAssignment(Vertex current) {
		assignList.add(current);
		String lable = current.getLabel();
		if (lable.contains("=")) {
			String[] values = lable.split("=");
			String lhs = values[0];
			if (lhs.contains("[")) {
				lhs = handleArray(lhs);
			}
			lhs = lhs.trim();
			Vertex lastAssignVertex = null;
			if (lhs.contains(",")) {
				String[] temp = lhs.split(",");
				for (int i = 0; i < temp.length; i++) {
					lastAssignVertex = null;
					if (lastAssignment.containsKey(temp[i])) {
						lastAssignVertex = lastAssignment.get(temp[i]);
					}
					if (lastAssignVertex != null) {
						createEdge(lastAssignVertex, current, "data");
					}
					if (i == 0) {
						lastAssignment.put(temp[i], current);
						addToLastAssignmentVertex(current);
					}
				}
			} else {
				lastAssignVertex = null;
				if (lastAssignment.containsKey(lhs)) {
					lastAssignVertex = lastAssignment.get(lhs);
				}
				lastAssignment.put(lhs, current);
				addToLastAssignmentVertex(current);
				if (lastAssignVertex != null) {
					createEdge(lastAssignVertex, current, "data");
				}
			}
			if (values.length == 1) {
				return;
			}
			String rhs = values[1];
			if (logger) {
				System.out.println("RHS");
				System.out.println(rhs);
			}
			String splitstring = "";
			String replaced = split(rhs);
			if (!(replaced.equalsIgnoreCase(rhs))) {
				splitstring = " ";
				rhs = replaced;
			}
			if (splitstring == "") {
				if (rhs.contains("[")) {
					rhs = handleArray(rhs);
				}
				rhs = rhs.trim();
				if (rhs.contains("new")) {
					return;
				}
				if (rhs.contains(".")) {
					rhs = rhs.substring(0, rhs.indexOf("."));
				}
				if (rhs.contains(",")) {
					String[] temp = rhs.split(",");
					for (int i = 0; i < temp.length; i++) {
						lastAssignVertex = null;
						if (lastAssignment.containsKey(temp[i])) {
							lastAssignVertex = lastAssignment.get(temp[i]);
						}
						if (lastAssignVertex != null) {
							createEdge(lastAssignVertex, current, "data");
						}
					}
				} else {
					lastAssignVertex = null;
					if (lastAssignment.containsKey(rhs)) {
						lastAssignVertex = lastAssignment.get(rhs);
					}
					if (lastAssignVertex != null) {
						createEdge(lastAssignVertex, current, "data");
					}
				}
			} else {
				if (logger) {
					System.out.println("RHS");
					System.out.println(rhs);
					System.out.println("AHAHAHAHHA");
					System.out.println("splitstring" + splitstring);
				}
				String rhsTemp[] = rhs.split(splitstring);
				for (int i = 0; i < rhsTemp.length; i++) {
					if (rhsTemp[i].contains("[")) {
						rhsTemp[i] = handleArray(rhsTemp[i]);
					}
				}
				for (int h = 0; h < rhsTemp.length; h++) {
					if (rhsTemp[h].contains(",")) {
						String[] temp = rhsTemp[h].split(",");
						for (int i = 0; i < temp.length; i++) {
							if (logger) {
								System.out.println(temp[i]);
							}
							lastAssignVertex = null;
							if (lastAssignment.containsKey(temp[i])) {
								lastAssignVertex = lastAssignment.get(temp[i]);
							}
							if (lastAssignVertex != null) {
								createEdge(lastAssignVertex, current, "data");
							}
						}
					} else {
						lastAssignVertex = null;
						if (lastAssignment.containsKey(rhsTemp[h])) {
							lastAssignVertex = lastAssignment.get(rhsTemp[h]);
						}
						if (lastAssignVertex != null) {
							createEdge(lastAssignVertex, current, "data");
						}
					}
				}
			}
		} else {
			lastAssignment.put(current.getLabel(), current);
		}
		if (isInControlLoop()) {
			createEdge(controlstack.peek(), current, "control");
		}
	}

	private String handleArray(String currentExp) {
		String index = currentExp.substring(currentExp.indexOf("["),
				currentExp.indexOf("]"));
		index = index.trim();
		currentExp = currentExp.substring(0, currentExp.indexOf("["));
		currentExp = currentExp.trim();
		if (index != null || index != "") {
			currentExp = currentExp + "," + index;
		}
		return currentExp;
	}

	private String split(String currentExp) {
		if (currentExp.contains("-")) {
			currentExp = currentExp.replace("-", " ");
		} else if (currentExp.contains("+")) {
			currentExp = currentExp.replace("+", " ");
		} else if (currentExp.contains("*")) {
			currentExp = currentExp.replace("*", " ");
		} else if (currentExp.contains("/")) {
			currentExp = currentExp.replace("/", " ");
		} else if (currentExp.contains(".")) {
			currentExp = currentExp.replace(".", " ");
		} else if (currentExp.contains("%")) {
			currentExp = currentExp.replace("%", " ");
		}
		return currentExp;
	}

	private boolean isInControlLoop() {
		if (controlstack.isEmpty()) {
			return false;
		}
		Vertex controlVertex = controlstack.peek();
		String vertexLabel = controlVertex.getLabel();
		if (vertexLabel.toLowerCase().contains("while")) {
			return enterWhile;
		} else if (vertexLabel.toLowerCase().contains("for")) {
			return enterFor;
		} else if (vertexLabel.toLowerCase().contains("if")) {
			return enterIf;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodHeader(Java8Parser.MethodHeaderContext ctx) {

		/*
		 * pushing the method name to the stack
		 */
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("METHOD HEADER");
			System.out.println("BEFORE");
			printStuff();
			System.out.println(ctx.getText());
		}
		method.push(ctx.getChild(1).getText()
				.substring(0, ctx.getChild(1).getText().indexOf("(")));
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFormalParameter(Java8Parser.FormalParameterContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("ENTER FORMAL PARAM line 374");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().contains(function_name)) {

			String current = ctx.getText();
			if (logger) {
				System.out.println("Single formal parameter "
						+ ctx.getChild(1).getText());
			}
			Vertex decl = new Vertex(current, "decl");
			currentVertex = decl;
			declList.add(decl);
			lastAssignment.put(ctx.getChild(1).getText(), decl);
			addToLastAssignmentVertex(decl);
			addType(ctx.getChild(1).getText(), current);
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	private void addType(String variableName, String currentDecl) {
		if (currentDecl.contains("=")) {
			currentDecl = currentDecl.split("=")[0];
		}
		currentDecl = currentDecl.substring(0,
				currentDecl.lastIndexOf(variableName));
		if (currentDecl.contains("[")) {
			currentDecl = currentDecl.substring(0, currentDecl.indexOf("["));
			currentDecl = currentDecl + " array";
		}
		type.put(variableName, currentDecl);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodBody(Java8Parser.MethodBodyContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("METHOD BODY line 394");
			System.out.println("BEFORE");
			printStuff();
			System.out.println(ctx.getText());
			System.out.println("Method Body" + ctx.getText());
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodBody(Java8Parser.MethodBodyContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("EXIT METHOD BODY");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		method.pop();
		if (logger) {
			System.out.println(" EXIT Method Header " + ctx.getText());
			System.out.println("AFTER");
			printStuff();

		}
	}

	private boolean checkForControl(String current) {
		boolean check = false;
		String temp = current.substring(current.indexOf("(") + 1,
				current.indexOf("{") - 1);
		int countSemi = temp.length() - temp.replace(";", "").length();
		if (countSemi >= 2) {
			temp = temp.substring(temp.indexOf(";") + 1, temp.lastIndexOf(";"));
		}
		for (int i = 0; i < temp.length(); i++) {
			int u = temp.charAt(i);
			if ((u == 60 || u == 61 || u == 62) && !(temp.contains("=="))) {
				check = true;
				break;
			} else {
				check = false;
			}
		}
		return check;
	}

	public void checkAndAddEdgeForControl(Vertex control) {
		if (controlstack.isEmpty()) {
			return;
		}
		Vertex stack_top = controlstack.peek();
		String vertexLabel = stack_top.getLabel();
		if (vertexLabel.toLowerCase().contains("while")) {
			if (enterWhile) {
				createEdge(stack_top, control, "control");
			}
		} else if (vertexLabel.toLowerCase().contains("for")) {
			if (enterFor) {
				createEdge(stack_top, control, "control");
			}
		} else if (vertexLabel.toLowerCase().contains("if")) {
			if (enterIf) {
				createEdge(stack_top, control, "control");
			}
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterIfThenStatement(Java8Parser.IfThenStatementContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("ENTER IF THEN");
			System.out.println("BEFORE");
			printStuff();
			System.out.println(ctx.getText());
		}
		if (method.peek().toString().equals(function_name)) {
			var_if = true;
			if_count++;
			controlAndCountStack.push("if _if_" + if_count);
			String current = ctx.getText();
			boolean check = checkForControl(current);
			if (check) {
				enterIf = true;
			} else {
				enterIf = true;
				String[] ifSt = controlAndCountStack.peek().split(" ");
				Vertex control = new Vertex(ifSt[0]
						+ " "
						+ current.substring(current.indexOf("(") + 1,
								current.indexOf("{") - 1) + "" + ifSt[1],
						"control");
				currentVertex = control;
				checkAndAddEdgeForControl(control);
				addEdgesForUniqueControl(current, control);
				controlList.add(control);
				controlstack.push(control);
			}
		}
		if (logger) {
			System.out.println("AAFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitIfThenStatement(Java8Parser.IfThenStatementContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("EXIT IF THEN");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			if (logger) {
				System.out.println("popping from If");
			}
			var_if = false;
			enterIf = false;
			controlAndCountStack.pop();
			// if_count--;
			controlstack.pop();
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterIfThenElseStatement(
			Java8Parser.IfThenElseStatementContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("ENTER IF THEN ELSE");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			var_if = true;
			if_count++;
			controlAndCountStack.push("if _if_" + if_count);
			String current = ctx.getText();
			boolean check = checkForControl(current);
			if (check) {
				enterIf = true;
			} else {
				enterIf = true;
				String[] ifSt = controlAndCountStack.peek().split(" ");
				Vertex control = new Vertex(ifSt[0]
						+ " "
						+ current.substring(current.indexOf("(") + 1,
								current.indexOf("{") - 1) + "" + ifSt[1],
						"control");
				currentVertex = control;
				checkAndAddEdgeForControl(control);
				controlList.add(control);
				controlstack.push(control);
			}
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();

		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitIfThenElseStatement(
			Java8Parser.IfThenElseStatementContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("EXIT IF THEN ELSE");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			var_if = false;
			enterIf = false;
			controlAndCountStack.pop();
			controlstack.pop();
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterIfThenElseStatementNoShortIf(
			Java8Parser.IfThenElseStatementNoShortIfContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("ENTER IF THEN ELSE");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			var_if = true;
			if_count++;
			controlAndCountStack.push("if _if_" + if_count);
			String current = ctx.getText();
			boolean check = checkForControl(current);
			if (check) {
				enterIf = true;
			} else {
				enterIf = true;
				String[] ifSt = controlAndCountStack.peek().split(" ");
				Vertex control = new Vertex(ifSt[0]
						+ " "
						+ current.substring(current.indexOf("(") + 1,
								current.indexOf("{") - 1) + "" + ifSt[1],
						"control");
				currentVertex = control;
				checkAndAddEdgeForControl(control);
				controlList.add(control);
				controlstack.push(control);
			}
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitIfThenElseStatementNoShortIf(
			Java8Parser.IfThenElseStatementNoShortIfContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("EXIT IF THEN ELSE");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
			System.out.println("Popping from If");
		}
		var_if = false;
		enterIf = false;
		controlAndCountStack.pop();
		controlstack.pop();
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterWhileStatement(Java8Parser.WhileStatementContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("ENTER WHILE STATEMNT");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			var_while = true;
			while_count++;
			controlAndCountStack.push("whileloop _while_" + while_count);
			String current = ctx.getText();
			boolean check = checkForControl(current);
			if (check) {
				enterWhile = true;
			} else {
				enterWhile = true;
				String[] whileSt = controlAndCountStack.peek().split(" ");
				Vertex control = new Vertex(whileSt[0]
						+ " "
						+ current.substring(current.indexOf("(") + 1,
								current.indexOf("{") - 1) + "" + whileSt[1],
						"control");
				currentVertex = control;
				checkAndAddEdgeForControl(control);
				controlList.add(control);
				controlstack.push(control);
			}
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitWhileStatement(Java8Parser.WhileStatementContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("EXIT WHILE STATEMENT");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			if (logger) {
				System.out.println("Popping from while");
			}
			var_while = false;
			enterWhile = false;
			controlAndCountStack.pop();
			// while_count--;
			controlstack.pop();
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterForStatement(Java8Parser.ForStatementContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("ENTER FOR STATEMENT");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			var_for = true;
			for_count++;
			controlAndCountStack.push("forloop _for_" + for_count);

			String current = ctx.getText();
			boolean check = checkForControl(current);
			if (check) {
				enterFor = true;
			} else {
				enterFor = true;
				String[] forSt = controlAndCountStack.peek().split(" ");
				Vertex for_v = new Vertex(forSt[0]
						+ " "
						+ current.substring(current.indexOf("(") + 1,
								current.indexOf("{") - 1) + "" + forSt[1],
						"control");
				currentVertex = for_v;
				checkAndAddEdgeForControl(for_v);
				controlList.add(for_v);
				controlstack.push(for_v);
			}
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitForStatement(Java8Parser.ForStatementContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("Exiting for statement " + ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			var_for = false;
			enterFor = false;
			controlAndCountStack.pop();
			// for_count--;
			if (logger) {
				System.out.println("Popping from from ");
			}
			controlstack.pop();
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterForUpdate(Java8Parser.ForUpdateContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("ENTER FOR UPDATE");
			System.out.println(ctx.getText());
		}
		if (method.peek().toString().equals(function_name)) {
			ForUpdate = true;
		}
	}

	private void addEdgesForUniqueControl(String current, Vertex control) {
		current = current.substring(current.indexOf("(") + 1,
				current.indexOf("{") - 1);
		int countSemi = current.length() - current.replace(";", "").length();
		if (countSemi >= 2) {
			current = current.substring(current.indexOf(";") + 1,
					current.lastIndexOf(";"));
		}
		String splitter = "";
		if (current.contains(">")) {
			splitter = ">";
		} else if (current.contains("<")) {
			splitter = "<";
		} else if (current.contains(">=")) {
			splitter = ">=";
		} else if (current.contains("<=")) {
			splitter = "<=";
		} else if (current.contains("==")) {
			splitter = "==";
		} else if (current.contains("!=")) {
			splitter = "!=";
		}
		if (splitter == "") {
			return;
		}
		current = current.replace(splitter, " ");
		String[] temp = current.split(" ");
		handleOneSideOfRelationalExpression(temp[0], control);
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterReturnStatement(Java8Parser.ReturnStatementContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("ENTER RETURN STATEMENT");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
			System.out.println("RETURN " + ctx.getChild(1).getText());
		}
		Vertex ret = null;
		if (method.peek().toString().equals(function_name)) {
			ret = new Vertex(ctx.getChild(1).getText(), "return");
			returnList.add(ret);

			if (ctx.getChild(1).getText().contains("[")) {
				String array_name = ctx.getChild(1).getText()
						.substring(0, ctx.getChild(1).getText().indexOf("["));

				for (int i = 0; i < declList.size(); i++) {
					String temp = declList
							.get(i)
							.toString()
							.substring(0,
									declList.get(i).toString().indexOf("["))
							.trim();
					if (temp.equals(array_name)) {
						createEdge(declList.get(i), ret, "data");
						break;
					}
				}

			} else {
				String name = ctx.getChild(1).getText();
				for (int i = 0; i < declList.size(); i++) {
					String temp = declList.get(i).toString().trim();
					if (temp.equals(name)) {
						createEdge(declList.get(i), ret, "data");
						break;
					}
				}
			}
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodInvocation(Java8Parser.MethodInvocationContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("Enter MEthod Invocation");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			if (logger) {
				System.out.println("Stack content " + controlstack);
				System.out.println("METHOD INVOCATION " + ctx.getText());
			}
			if (ctx.getText().contains("System.out.")) {
				Vertex exp = new Vertex(ctx.getText(), "expr");
				currentVertex = exp;
				exprList.add(exp);
				if (!controlstack.empty()) {
					createEdge(controlstack.peek(), exp, "control");
					enterMethod = true;
					expression = exp;

				} else {
					enterMethod = true;
					expression = exp;
				}

			} else if (ctx.getText().contains("$")) {
				handleFunction(ctx.getText());

			} else {
				if (logger) {
					System.out.println("Call site " + ctx.getText());
				}
				Vertex call_site = new Vertex(ctx.getText(), "call-site");
				currentVertex = call_site;
				callSiteList.add(call_site);
				if (ctx.getText().contains(".")) {

					String object = ctx.getText().substring(0,
							ctx.getText().indexOf("."));

					for (int i = 0; i < declList.size(); i++) {
						if (declList.get(i).toString().equals(object)) {
							createEdge(declList.get(i), call_site, "data");
							break;
						}
					}
				}

				if (!controlstack.empty()) {
					createEdge(controlstack.peek(), call_site, "control");
					enterMethod = true;
					expression = call_site;
				} else {
					if (logger) {
						System.out.println("SPECIAL ONE ");
					}
					enterMethod = true;
					expression = call_site;
				}
			}
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	public void handleFunction(String current) {
		return;
		// if (current.contains("get")) {
		// String temp = current.substring(current.indexOf("(") + 1,
		// current.indexOf(")"));
		// temp = temp.trim();
		// String[] variables = temp.split(",");
		// String expression = "rt=" + variables[0] + "[" + variables[1] + "]";
		// addToAssignment(new Vertex(expression, "assign"));
		// }
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAssignment(Java8Parser.AssignmentContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("Enter assignment");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			Vertex assignNode = new Vertex(ctx.getText(), "assign");
			currentVertex = assignNode;
			addToAssignment(assignNode);
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterRelationalExpression(
			Java8Parser.RelationalExpressionContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("enterRELATION EXP");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		if (method.peek().toString().equals(function_name)) {
			if (ctx.getText().contains(">") || ctx.getText().contains("<")
					|| ctx.getText().contains(">=")
					|| ctx.getText().contains("<=")
					|| ctx.getText().contains("==")
					|| ctx.getText().contains("!=")) {

				if (ctx.getText().contains("!=")) {
					System.out.println("GOT U ");
				}
				if (!(ctx.getText().contains("new"))) {
					if (controlAndCountStack.isEmpty()) {
						return;
					}
					Vertex control = null;
					String[] peekValue = controlAndCountStack.peek().split(" ");
					if (controlstack.empty()) {
						control = new Vertex(peekValue[0] + " " + ctx.getText()
								+ peekValue[1], "control");
						controlstack.push(control);
						controlList.add(control);
					} else {
						Vertex stack_top = controlstack.peek();
						String vertexLabel = stack_top.getLabel();
						control = new Vertex(peekValue[0] + " " + ctx.getText()
								+ peekValue[1], "control");
						controlList.add(control);
						controlstack.push(control);
						if (vertexLabel.toLowerCase().contains("while")) {
							if (enterWhile) {
								createEdge(stack_top, control, "control");
							}
						} else if (vertexLabel.toLowerCase().contains("for")) {
							if (enterFor) {
								createEdge(stack_top, control, "control");
							}
						} else if (vertexLabel.toLowerCase().contains("if")) {
							if (enterIf) {
								createEdge(stack_top, control, "control");
							}
						}
					}
					if (logger) {
						System.out.println("CHILD  "
								+ ctx.getChild(1).getText());
					}
					String lhs = ctx.getText().substring(0,
							ctx.getText().indexOf(ctx.getChild(1).getText()));
					lhs = lhs.trim();
					handleOneSideOfRelationalExpression(lhs, control);

					String rhs = ctx.getText()
							.substring(
									ctx.getText().indexOf(
											ctx.getChild(1).getText()) + 1);
					rhs = rhs.trim();
					if (logger) {
						System.out.println("RHS" + rhs);
					}
					handleOneSideOfRelationalExpression(rhs, control);
				}
			} else {
				String temp = split(ctx.getText());
				if (temp.equalsIgnoreCase(ctx.getText())) {
					if (temp.contains("[")) {
						temp = handleArray(temp);
						String[] temps = temp.split(",");
						for (int i = 0; i < temps.length; i++) {
							checkAndAddEdge(temp, currentVertex);
						}
					} else if (temp.contains("new")) {
						return;
					} else {
						checkAndAddEdge(temp, currentVertex);
					}
				} else {
					String[] temps = temp.split(" ");
					for (int i = 0; i < temps.length; i++) {
						if (temps[i].contains("[")) {
							temps[i] = handleArray(temps[i]);
						}
						if (temps[i].contains("new")) {
							continue;
						}
						if (temps[i].contains(",")) {
							String[] tempp = temps[i].split(" ");
							for (int j = 0; j < tempp.length; j++) {
								checkAndAddEdge(tempp[j], currentVertex);
							}
						} else {
							checkAndAddEdge(temps[i], currentVertex);
						}
					}
				}
			}
		}
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	private void handleOneSideOfRelationalExpression(String expression,
			Vertex control) {
		String splitstring = "";
		String replaced = split(expression);
		if (!(replaced.equalsIgnoreCase(expression))) {
			splitstring = " ";
			expression = replaced;
		}
		if (splitstring != "") {
			String tempExpression[] = expression.split(splitstring);
			/*
			 * to handle strings array "[]"
			 */
			for (int i = 0; i < tempExpression.length; i++) {
				if (tempExpression[i].contains("[")) {
					tempExpression[i] = handleArray(tempExpression[i]);
				}
			}
			for (int h = 0; h < tempExpression.length; h++) {
				if (tempExpression[h].contains(",")) {
					String[] temp = tempExpression[h].split(",");
					for (int i = 0; i < temp.length; i++) {
						checkAndAddEdge(temp[i], control);
					}
				} else {
					checkAndAddEdge(tempExpression[h], control);
				}
			}
		} else {
			if (expression.contains("[")) {
				expression = handleArray(expression);
				String[] temp = expression.split(",");
				for (int i = 0; i < temp.length; i++) {
					checkAndAddEdge(temp[i], control);
				}
			} else {
				checkAndAddEdge(expression, control);
			}
		}
	}

	private void checkAndAddEdge(String current, Vertex currVertex) {
		if (lastAssignment.containsKey(current)) {
			createEdge(lastAssignment.get(current), currVertex, "data");
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPreIncrementExpression(
			Java8Parser.PreIncrementExpressionContext ctx) {
		if (logger) {
			System.out.println("BEFORE");
			printStuff();
			System.out.println("ENTER PRE INCREMENT EXPR");
			System.out.println(ctx.getText());
		}
		commonIncDecrement(new Vertex(preProcessIncDecrementPre(ctx.getText(),
				"+"), "assign"));
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPreDecrementExpression(
			Java8Parser.PreDecrementExpressionContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("ENTER PREDECREMENT");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		commonIncDecrement(new Vertex(preProcessIncDecrementPre(ctx.getText(),
				"-"), "assign"));
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPostIncrementExpression(
			Java8Parser.PostIncrementExpressionContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("ENTER POST INCREMENT");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		commonIncDecrement(new Vertex(preProcessIncDecrementPost(ctx.getText(),
				"+"), "assign"));
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPostDecrementExpression(
			Java8Parser.PostDecrementExpressionContext ctx) {
		if (logger) {
			System.out.println("Called " + countCount);
			countCount++;
			System.out.println("ENTER POST DECREMENT");
			System.out.println(ctx.getText());
			System.out.println("BEFORE");
			printStuff();
		}
		commonIncDecrement(new Vertex(preProcessIncDecrementPost(ctx.getText(),
				"-"), "assign"));
		if (logger) {
			System.out.println("AFTER");
			printStuff();
		}
	}

	private String preProcessIncDecrementPost(String original, String operator) {
		String preprocessed = original.substring(0, original.indexOf(operator));
		preprocessed = preprocessed + "=" + preprocessed + operator + "1";
		return preprocessed;
	}

	private String preProcessIncDecrementPre(String original, String operator) {
		String preprocessed = original
				.substring(original.indexOf(operator) + 1);
		preprocessed = preprocessed + "=" + preprocessed + operator + "1";
		return preprocessed;
	}

	private void commonIncDecrement(Vertex current) {
		if (method.peek().toString().equals(function_name)) {
			addToAssignment(current);
			// incList.add(current);
			String controlStackTop = controlstack.peek().toString();
			if ((controlStackTop.contains("for") && enterFor)
					|| ((controlStackTop.contains("while") || controlstack
							.peek().toString().contains("do")) && enterWhile)
					|| (controlstack.peek().toString().contains("if") && enterIf)) {
				createEdge(controlstack.peek(), current, "control");
			}
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLiteral(Java8Parser.LiteralContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLiteral(Java8Parser.LiteralContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterType(Java8Parser.TypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitType(Java8Parser.TypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPrimitiveType(Java8Parser.PrimitiveTypeContext ctx) {
		if (logger) {
			System.out.println("Line number " + "47 " + "declaration "
					+ ctx.getText());
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPrimitiveType(Java8Parser.PrimitiveTypeContext ctx) {

		// System.out.println("Primitive Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterNumericType(Java8Parser.NumericTypeContext ctx) {
		// System.out.println("Line Number"+" 66 "+"declaration "
		// +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitNumericType(Java8Parser.NumericTypeContext ctx) {
		// System.out.println("Primitive Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterIntegralType(Java8Parser.IntegralTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitIntegralType(Java8Parser.IntegralTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFloatingPointType(Java8Parser.FloatingPointTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFloatingPointType(Java8Parser.FloatingPointTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterReferenceType(Java8Parser.ReferenceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitReferenceType(Java8Parser.ReferenceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassOrInterfaceType(
			Java8Parser.ClassOrInterfaceTypeContext ctx) {
		// System.out.println("Class Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassOrInterfaceType(
			Java8Parser.ClassOrInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassType(Java8Parser.ClassTypeContext ctx) {
		// System.out.println("Class Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassType(Java8Parser.ClassTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassType_lf_classOrInterfaceType(
			Java8Parser.ClassType_lf_classOrInterfaceTypeContext ctx) {
		// System.out.println("Class Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassType_lf_classOrInterfaceType(
			Java8Parser.ClassType_lf_classOrInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassType_lfno_classOrInterfaceType(
			Java8Parser.ClassType_lfno_classOrInterfaceTypeContext ctx) {
		// System.out.println("Class Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassType_lfno_classOrInterfaceType(
			Java8Parser.ClassType_lfno_classOrInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInterfaceType(Java8Parser.InterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInterfaceType(Java8Parser.InterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInterfaceType_lf_classOrInterfaceType(
			Java8Parser.InterfaceType_lf_classOrInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInterfaceType_lf_classOrInterfaceType(
			Java8Parser.InterfaceType_lf_classOrInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInterfaceType_lfno_classOrInterfaceType(
			Java8Parser.InterfaceType_lfno_classOrInterfaceTypeContext ctx) {
		// System.out.println("Class Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInterfaceType_lfno_classOrInterfaceType(
			Java8Parser.InterfaceType_lfno_classOrInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeVariable(Java8Parser.TypeVariableContext ctx) {
		// System.out.println("Line number "+"218 "+"Type Variable"
		// +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeVariable(Java8Parser.TypeVariableContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterArrayType(Java8Parser.ArrayTypeContext ctx) {
		System.out.println("Array type " + ctx.getText());
		// System.out.println("Array Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitArrayType(Java8Parser.ArrayTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDims(Java8Parser.DimsContext ctx) {
		// System.out.println("Dim Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDims(Java8Parser.DimsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeParameter(Java8Parser.TypeParameterContext ctx) {
		System.out.println("Line number " + "264 " + "TypeParameter "
				+ ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeParameter(Java8Parser.TypeParameterContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeParameterModifier(
			Java8Parser.TypeParameterModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeParameterModifier(
			Java8Parser.TypeParameterModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeBound(Java8Parser.TypeBoundContext ctx) {
		System.out.println("Bound Type " + ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeBound(Java8Parser.TypeBoundContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAdditionalBound(Java8Parser.AdditionalBoundContext ctx) {
		System.out.println("Additional Bound Type " + ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAdditionalBound(Java8Parser.AdditionalBoundContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeArguments(Java8Parser.TypeArgumentsContext ctx) {
		if (logger) {
			System.out.println("Line number " + "321 " + "Type Arguments "
					+ ctx.getText());
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeArguments(Java8Parser.TypeArgumentsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeArgumentList(Java8Parser.TypeArgumentListContext ctx) {
		// System.out.println("Line Number "+"335 "+"enterTypeArgumentList Type "
		// +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeArgumentList(Java8Parser.TypeArgumentListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */

	@Override
	public void enterTypeArgument(Java8Parser.TypeArgumentContext ctx) {
		// System.out.println("Line number "+"349 "+"enterTypeArgument Type "
		// +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeArgument(Java8Parser.TypeArgumentContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterWildcard(Java8Parser.WildcardContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitWildcard(Java8Parser.WildcardContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterWildcardBounds(Java8Parser.WildcardBoundsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitWildcardBounds(Java8Parser.WildcardBoundsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPackageName(Java8Parser.PackageNameContext ctx) {
		// System.out.println("Packagename Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPackageName(Java8Parser.PackageNameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeName(Java8Parser.TypeNameContext ctx) {
		// System.out.println("TypeName  " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeName(Java8Parser.TypeNameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPackageOrTypeName(Java8Parser.PackageOrTypeNameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPackageOrTypeName(Java8Parser.PackageOrTypeNameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitExpressionName(Java8Parser.ExpressionNameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodName(Java8Parser.MethodNameContext ctx) {
		if (logger) {
			System.out.println("Line Number " + "449 " + "Method Name "
					+ ctx.getText());
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodName(Java8Parser.MethodNameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAmbiguousName(Java8Parser.AmbiguousNameContext ctx) {
		if (logger) {
			System.out.println("Ambigous Name " + ctx.getText());
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAmbiguousName(Java8Parser.AmbiguousNameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterCompilationUnit(Java8Parser.CompilationUnitContext ctx) {

		// System.out.println("Compilation Name " +ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitCompilationUnit(Java8Parser.CompilationUnitContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPackageDeclaration(
			Java8Parser.PackageDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPackageDeclaration(Java8Parser.PackageDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPackageModifier(Java8Parser.PackageModifierContext ctx) {
		// System.out.println("PackageModifier Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPackageModifier(Java8Parser.PackageModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterImportDeclaration(Java8Parser.ImportDeclarationContext ctx) {
		// System.out.println("Import Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitImportDeclaration(Java8Parser.ImportDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSingleTypeImportDeclaration(
			Java8Parser.SingleTypeImportDeclarationContext ctx) {
		// System.out.println("Import Type " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSingleTypeImportDeclaration(
			Java8Parser.SingleTypeImportDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeImportOnDemandDeclaration(
			Java8Parser.TypeImportOnDemandDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeImportOnDemandDeclaration(
			Java8Parser.TypeImportOnDemandDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSingleStaticImportDeclaration(
			Java8Parser.SingleStaticImportDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSingleStaticImportDeclaration(
			Java8Parser.SingleStaticImportDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterStaticImportOnDemandDeclaration(
			Java8Parser.StaticImportOnDemandDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitStaticImportOnDemandDeclaration(
			Java8Parser.StaticImportOnDemandDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeDeclaration(Java8Parser.TypeDeclarationContext ctx) {
		// System.out.println("TypeDeclaration " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeDeclaration(Java8Parser.TypeDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassDeclaration(Java8Parser.ClassDeclarationContext ctx) {
		// System.out.println(" enterClassDeclaration " +ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassDeclaration(Java8Parser.ClassDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterNormalClassDeclaration(
			Java8Parser.NormalClassDeclarationContext ctx) {

		// System.out.println(" enterClassDeclaration " +ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitNormalClassDeclaration(
			Java8Parser.NormalClassDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassModifier(Java8Parser.ClassModifierContext ctx) {

		// System.out.println(" ClassModifier " +ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassModifier(Java8Parser.ClassModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeParameters(Java8Parser.TypeParametersContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeParameters(Java8Parser.TypeParametersContext ctx) {

		System.out.println("Line Number " + "652 " + "TypeParamter "
				+ ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeParameterList(Java8Parser.TypeParameterListContext ctx) {

		System.out.println(" ParameterList " + ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeParameterList(Java8Parser.TypeParameterListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSuperclass(Java8Parser.SuperclassContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSuperclass(Java8Parser.SuperclassContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSuperinterfaces(Java8Parser.SuperinterfacesContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSuperinterfaces(Java8Parser.SuperinterfacesContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInterfaceTypeList(Java8Parser.InterfaceTypeListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInterfaceTypeList(Java8Parser.InterfaceTypeListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassBody(Java8Parser.ClassBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassBody(Java8Parser.ClassBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassBodyDeclaration(
			Java8Parser.ClassBodyDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassBodyDeclaration(
			Java8Parser.ClassBodyDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassMemberDeclaration(
			Java8Parser.ClassMemberDeclarationContext ctx) {
		// System.out.println(" Class Member " +ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassMemberDeclaration(
			Java8Parser.ClassMemberDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFieldDeclaration(Java8Parser.FieldDeclarationContext ctx) {

		// System.out.println(" ClassField " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFieldDeclaration(Java8Parser.FieldDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFieldModifier(Java8Parser.FieldModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFieldModifier(Java8Parser.FieldModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterVariableDeclaratorList(
			Java8Parser.VariableDeclaratorListContext ctx) {
		// System.out.println("Line Number" + "778 "+" Varaibledeclarationlist "
		// +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitVariableDeclaratorList(
			Java8Parser.VariableDeclaratorListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitVariableDeclarator(Java8Parser.VariableDeclaratorContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterVariableDeclaratorId(
			Java8Parser.VariableDeclaratorIdContext ctx) {
		// System.out.println("Line Number "+"806 "+" variabledeclartorID "
		// +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitVariableDeclaratorId(
			Java8Parser.VariableDeclaratorIdContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterVariableInitializer(
			Java8Parser.VariableInitializerContext ctx) {
		// System.out.println("Line Number "+"820 "+" assignment "
		// +ctx.getText()+"Parent "+ctx.getParent().getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitVariableInitializer(
			Java8Parser.VariableInitializerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannType(Java8Parser.UnannTypeContext ctx) {

		// System.out.println(" enterUnannType " +ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannType(Java8Parser.UnannTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannPrimitiveType(
			Java8Parser.UnannPrimitiveTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannPrimitiveType(Java8Parser.UnannPrimitiveTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannReferenceType(
			Java8Parser.UnannReferenceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannReferenceType(Java8Parser.UnannReferenceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannClassOrInterfaceType(
			Java8Parser.UnannClassOrInterfaceTypeContext ctx) {

		// System.out.println("Class Name "+ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannClassOrInterfaceType(
			Java8Parser.UnannClassOrInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannClassType(Java8Parser.UnannClassTypeContext ctx) {
		// System.out.println("Class Name "+ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannClassType(Java8Parser.UnannClassTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannClassType_lf_unannClassOrInterfaceType(
			Java8Parser.UnannClassType_lf_unannClassOrInterfaceTypeContext ctx) {
		// System.out.println("Class Name "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannClassType_lf_unannClassOrInterfaceType(
			Java8Parser.UnannClassType_lf_unannClassOrInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannClassType_lfno_unannClassOrInterfaceType(
			Java8Parser.UnannClassType_lfno_unannClassOrInterfaceTypeContext ctx) {
		// System.out.println("Class Name "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannClassType_lfno_unannClassOrInterfaceType(
			Java8Parser.UnannClassType_lfno_unannClassOrInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannInterfaceType(
			Java8Parser.UnannInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannInterfaceType(Java8Parser.UnannInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannInterfaceType_lf_unannClassOrInterfaceType(
			Java8Parser.UnannInterfaceType_lf_unannClassOrInterfaceTypeContext ctx) {
		// System.out.println("Class Name "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannInterfaceType_lf_unannClassOrInterfaceType(
			Java8Parser.UnannInterfaceType_lf_unannClassOrInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannInterfaceType_lfno_unannClassOrInterfaceType(
			Java8Parser.UnannInterfaceType_lfno_unannClassOrInterfaceTypeContext ctx) {
		// System.out.println("Class Name "+ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannInterfaceType_lfno_unannClassOrInterfaceType(
			Java8Parser.UnannInterfaceType_lfno_unannClassOrInterfaceTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannTypeVariable(Java8Parser.UnannTypeVariableContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannTypeVariable(Java8Parser.UnannTypeVariableContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnannArrayType(Java8Parser.UnannArrayTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnannArrayType(Java8Parser.UnannArrayTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodDeclaration(Java8Parser.MethodDeclarationContext ctx) {

		// System.out.println("METHOD DECLARATION  " + ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodDeclaration(Java8Parser.MethodDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodModifier(Java8Parser.MethodModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodModifier(Java8Parser.MethodModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodHeader(Java8Parser.MethodHeaderContext ctx) {

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterResult(Java8Parser.ResultContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitResult(Java8Parser.ResultContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodDeclarator(Java8Parser.MethodDeclaratorContext ctx) {

		// System.out.println("FUNCTION_NAME "+function_name);
		if (logger) {
			System.out.println("METHOD DECLARATOR " + ctx.getText());
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodDeclarator(Java8Parser.MethodDeclaratorContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFormalParameterList(
			Java8Parser.FormalParameterListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFormalParameterList(
			Java8Parser.FormalParameterListContext ctx) {
		// System.out.println("FORMAL PARAMETER "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFormalParameters(Java8Parser.FormalParametersContext ctx) {
		// System.out.println("FORRMAL PARAMETERS " +ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFormalParameters(Java8Parser.FormalParametersContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFormalParameter(Java8Parser.FormalParameterContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterVariableModifier(Java8Parser.VariableModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitVariableModifier(Java8Parser.VariableModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLastFormalParameter(
			Java8Parser.LastFormalParameterContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLastFormalParameter(
			Java8Parser.LastFormalParameterContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterReceiverParameter(Java8Parser.ReceiverParameterContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitReceiverParameter(Java8Parser.ReceiverParameterContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterThrows_(Java8Parser.Throws_Context ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitThrows_(Java8Parser.Throws_Context ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterExceptionTypeList(Java8Parser.ExceptionTypeListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitExceptionTypeList(Java8Parser.ExceptionTypeListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterExceptionType(Java8Parser.ExceptionTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitExceptionType(Java8Parser.ExceptionTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInstanceInitializer(
			Java8Parser.InstanceInitializerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInstanceInitializer(
			Java8Parser.InstanceInitializerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterStaticInitializer(Java8Parser.StaticInitializerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitStaticInitializer(Java8Parser.StaticInitializerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterConstructorDeclaration(
			Java8Parser.ConstructorDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitConstructorDeclaration(
			Java8Parser.ConstructorDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterConstructorModifier(
			Java8Parser.ConstructorModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitConstructorModifier(
			Java8Parser.ConstructorModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterConstructorDeclarator(
			Java8Parser.ConstructorDeclaratorContext ctx) {

		System.out.println("CONSTRUCTOR DECLARATOR " + ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitConstructorDeclarator(
			Java8Parser.ConstructorDeclaratorContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSimpleTypeName(Java8Parser.SimpleTypeNameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSimpleTypeName(Java8Parser.SimpleTypeNameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterConstructorBody(Java8Parser.ConstructorBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitConstructorBody(Java8Parser.ConstructorBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterExplicitConstructorInvocation(
			Java8Parser.ExplicitConstructorInvocationContext ctx) {

		System.out.println(" CONSTRUCTOR INVOCATION " + ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitExplicitConstructorInvocation(
			Java8Parser.ExplicitConstructorInvocationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEnumDeclaration(Java8Parser.EnumDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEnumDeclaration(Java8Parser.EnumDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEnumBody(Java8Parser.EnumBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEnumBody(Java8Parser.EnumBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEnumConstantList(Java8Parser.EnumConstantListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEnumConstantList(Java8Parser.EnumConstantListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEnumConstant(Java8Parser.EnumConstantContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEnumConstant(Java8Parser.EnumConstantContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEnumConstantModifier(
			Java8Parser.EnumConstantModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEnumConstantModifier(
			Java8Parser.EnumConstantModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEnumBodyDeclarations(
			Java8Parser.EnumBodyDeclarationsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEnumBodyDeclarations(
			Java8Parser.EnumBodyDeclarationsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInterfaceDeclaration(
			Java8Parser.InterfaceDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInterfaceDeclaration(
			Java8Parser.InterfaceDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterNormalInterfaceDeclaration(
			Java8Parser.NormalInterfaceDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitNormalInterfaceDeclaration(
			Java8Parser.NormalInterfaceDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInterfaceModifier(Java8Parser.InterfaceModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInterfaceModifier(Java8Parser.InterfaceModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterExtendsInterfaces(Java8Parser.ExtendsInterfacesContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitExtendsInterfaces(Java8Parser.ExtendsInterfacesContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInterfaceBody(Java8Parser.InterfaceBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInterfaceBody(Java8Parser.InterfaceBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInterfaceMemberDeclaration(
			Java8Parser.InterfaceMemberDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInterfaceMemberDeclaration(
			Java8Parser.InterfaceMemberDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterConstantDeclaration(
			Java8Parser.ConstantDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitConstantDeclaration(
			Java8Parser.ConstantDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterConstantModifier(Java8Parser.ConstantModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitConstantModifier(Java8Parser.ConstantModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInterfaceMethodDeclaration(
			Java8Parser.InterfaceMethodDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInterfaceMethodDeclaration(
			Java8Parser.InterfaceMethodDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInterfaceMethodModifier(
			Java8Parser.InterfaceMethodModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInterfaceMethodModifier(
			Java8Parser.InterfaceMethodModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAnnotationTypeDeclaration(
			Java8Parser.AnnotationTypeDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAnnotationTypeDeclaration(
			Java8Parser.AnnotationTypeDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAnnotationTypeBody(
			Java8Parser.AnnotationTypeBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAnnotationTypeBody(Java8Parser.AnnotationTypeBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAnnotationTypeMemberDeclaration(
			Java8Parser.AnnotationTypeMemberDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAnnotationTypeMemberDeclaration(
			Java8Parser.AnnotationTypeMemberDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAnnotationTypeElementDeclaration(
			Java8Parser.AnnotationTypeElementDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAnnotationTypeElementDeclaration(
			Java8Parser.AnnotationTypeElementDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAnnotationTypeElementModifier(
			Java8Parser.AnnotationTypeElementModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAnnotationTypeElementModifier(
			Java8Parser.AnnotationTypeElementModifierContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDefaultValue(Java8Parser.DefaultValueContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDefaultValue(Java8Parser.DefaultValueContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAnnotation(Java8Parser.AnnotationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAnnotation(Java8Parser.AnnotationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterNormalAnnotation(Java8Parser.NormalAnnotationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitNormalAnnotation(Java8Parser.NormalAnnotationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterElementValuePairList(
			Java8Parser.ElementValuePairListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitElementValuePairList(
			Java8Parser.ElementValuePairListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterElementValuePair(Java8Parser.ElementValuePairContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitElementValuePair(Java8Parser.ElementValuePairContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterElementValue(Java8Parser.ElementValueContext ctx) {

		System.out.println("ELEMENT VALUE " + ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitElementValue(Java8Parser.ElementValueContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterElementValueArrayInitializer(
			Java8Parser.ElementValueArrayInitializerContext ctx) {
		System.out.println("Elemenet value Array initialization "
				+ ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitElementValueArrayInitializer(
			Java8Parser.ElementValueArrayInitializerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterElementValueList(Java8Parser.ElementValueListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitElementValueList(Java8Parser.ElementValueListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMarkerAnnotation(Java8Parser.MarkerAnnotationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMarkerAnnotation(Java8Parser.MarkerAnnotationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSingleElementAnnotation(
			Java8Parser.SingleElementAnnotationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSingleElementAnnotation(
			Java8Parser.SingleElementAnnotationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterArrayInitializer(Java8Parser.ArrayInitializerContext ctx) {
		// System.out.println("Array Parent "+ctx.getParent().getText()+" Child "+ctx.getChild(0).getText());
		if (logger) {
			System.out.println("Array initialization " + ctx.getText());
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitArrayInitializer(Java8Parser.ArrayInitializerContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterVariableInitializerList(
			Java8Parser.VariableInitializerListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitVariableInitializerList(
			Java8Parser.VariableInitializerListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterBlock(Java8Parser.BlockContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitBlock(Java8Parser.BlockContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterBlockStatements(Java8Parser.BlockStatementsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitBlockStatements(Java8Parser.BlockStatementsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterBlockStatement(Java8Parser.BlockStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitBlockStatement(Java8Parser.BlockStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLocalVariableDeclarationStatement(
			Java8Parser.LocalVariableDeclarationStatementContext ctx) {
		// System.out.println("Line number "+"1711 "+"decl, "+ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLocalVariableDeclarationStatement(
			Java8Parser.LocalVariableDeclarationStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLocalVariableDeclaration(
			Java8Parser.LocalVariableDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLocalVariableDeclaration(
			Java8Parser.LocalVariableDeclarationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterStatement(Java8Parser.StatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitStatement(Java8Parser.StatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterStatementNoShortIf(
			Java8Parser.StatementNoShortIfContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitStatementNoShortIf(Java8Parser.StatementNoShortIfContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterStatementWithoutTrailingSubstatement(
			Java8Parser.StatementWithoutTrailingSubstatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitStatementWithoutTrailingSubstatement(
			Java8Parser.StatementWithoutTrailingSubstatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEmptyStatement(Java8Parser.EmptyStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEmptyStatement(Java8Parser.EmptyStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLabeledStatement(Java8Parser.LabeledStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLabeledStatement(Java8Parser.LabeledStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLabeledStatementNoShortIf(
			Java8Parser.LabeledStatementNoShortIfContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLabeledStatementNoShortIf(
			Java8Parser.LabeledStatementNoShortIfContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterExpressionStatement(
			Java8Parser.ExpressionStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitExpressionStatement(
			Java8Parser.ExpressionStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterStatementExpression(
			Java8Parser.StatementExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitStatementExpression(
			Java8Parser.StatementExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAssertStatement(Java8Parser.AssertStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAssertStatement(Java8Parser.AssertStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSwitchStatement(Java8Parser.SwitchStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSwitchStatement(Java8Parser.SwitchStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSwitchBlock(Java8Parser.SwitchBlockContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSwitchBlock(Java8Parser.SwitchBlockContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSwitchBlockStatementGroup(
			Java8Parser.SwitchBlockStatementGroupContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSwitchBlockStatementGroup(
			Java8Parser.SwitchBlockStatementGroupContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSwitchLabels(Java8Parser.SwitchLabelsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSwitchLabels(Java8Parser.SwitchLabelsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSwitchLabel(Java8Parser.SwitchLabelContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSwitchLabel(Java8Parser.SwitchLabelContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEnumConstantName(Java8Parser.EnumConstantNameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEnumConstantName(Java8Parser.EnumConstantNameContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterWhileStatementNoShortIf(
			Java8Parser.WhileStatementNoShortIfContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitWhileStatementNoShortIf(
			Java8Parser.WhileStatementNoShortIfContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDoStatement(Java8Parser.DoStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDoStatement(Java8Parser.DoStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterForStatementNoShortIf(
			Java8Parser.ForStatementNoShortIfContext ctx) {
		// System.out.println("Line Number "+"2000 "+"For statement "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitForStatementNoShortIf(
			Java8Parser.ForStatementNoShortIfContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterBasicForStatement(Java8Parser.BasicForStatementContext ctx) {
		// System.out.println("Line Number "+"2014 "+"For statement "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitBasicForStatement(Java8Parser.BasicForStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterBasicForStatementNoShortIf(
			Java8Parser.BasicForStatementNoShortIfContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitBasicForStatementNoShortIf(
			Java8Parser.BasicForStatementNoShortIfContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterForInit(Java8Parser.ForInitContext ctx) {
		// System.out.println("Node: "+ ""+"for assign, "+ctx.getText());
		/*
		 * Vertex for_decl = new
		 * Vertex(ctx.getText().substring(0,ctx.getText().indexOf("=")),"decl");
		 * declList.add(for_decl); System.out.println("Created vertex "+
		 * for_decl.toString()); Vertex for_assign = new
		 * Vertex(ctx.getText().substring
		 * (ctx.getText().indexOf("=")-1),"assign"); assignList.add(for_assign);
		 * Edge for_decl_assign = new Edge(for_decl,for_assign,"data");
		 * System.out.println("Edge created "+for_decl_assign.toString());
		 */

		// enterforStructure=true;
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitForInit(Java8Parser.ForInitContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitForUpdate(Java8Parser.ForUpdateContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterStatementExpressionList(
			Java8Parser.StatementExpressionListContext ctx) {

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitStatementExpressionList(
			Java8Parser.StatementExpressionListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEnhancedForStatement(
			Java8Parser.EnhancedForStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEnhancedForStatement(
			Java8Parser.EnhancedForStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEnhancedForStatementNoShortIf(
			Java8Parser.EnhancedForStatementNoShortIfContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEnhancedForStatementNoShortIf(
			Java8Parser.EnhancedForStatementNoShortIfContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterBreakStatement(Java8Parser.BreakStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitBreakStatement(Java8Parser.BreakStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterContinueStatement(Java8Parser.ContinueStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitContinueStatement(Java8Parser.ContinueStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitReturnStatement(Java8Parser.ReturnStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterThrowStatement(Java8Parser.ThrowStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitThrowStatement(Java8Parser.ThrowStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterSynchronizedStatement(
			Java8Parser.SynchronizedStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitSynchronizedStatement(
			Java8Parser.SynchronizedStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTryStatement(Java8Parser.TryStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTryStatement(Java8Parser.TryStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterCatches(Java8Parser.CatchesContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitCatches(Java8Parser.CatchesContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterCatchClause(Java8Parser.CatchClauseContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitCatchClause(Java8Parser.CatchClauseContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterCatchFormalParameter(
			Java8Parser.CatchFormalParameterContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitCatchFormalParameter(
			Java8Parser.CatchFormalParameterContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterCatchType(Java8Parser.CatchTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitCatchType(Java8Parser.CatchTypeContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFinally_(Java8Parser.Finally_Context ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFinally_(Java8Parser.Finally_Context ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTryWithResourcesStatement(
			Java8Parser.TryWithResourcesStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTryWithResourcesStatement(
			Java8Parser.TryWithResourcesStatementContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterResourceSpecification(
			Java8Parser.ResourceSpecificationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitResourceSpecification(
			Java8Parser.ResourceSpecificationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterResourceList(Java8Parser.ResourceListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitResourceList(Java8Parser.ResourceListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterResource(Java8Parser.ResourceContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitResource(Java8Parser.ResourceContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPrimary(Java8Parser.PrimaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPrimary(Java8Parser.PrimaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPrimaryNoNewArray(Java8Parser.PrimaryNoNewArrayContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPrimaryNoNewArray(Java8Parser.PrimaryNoNewArrayContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPrimaryNoNewArray_lf_arrayAccess(
			Java8Parser.PrimaryNoNewArray_lf_arrayAccessContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPrimaryNoNewArray_lf_arrayAccess(
			Java8Parser.PrimaryNoNewArray_lf_arrayAccessContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPrimaryNoNewArray_lfno_arrayAccess(
			Java8Parser.PrimaryNoNewArray_lfno_arrayAccessContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPrimaryNoNewArray_lfno_arrayAccess(
			Java8Parser.PrimaryNoNewArray_lfno_arrayAccessContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPrimaryNoNewArray_lf_primary(
			Java8Parser.PrimaryNoNewArray_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPrimaryNoNewArray_lf_primary(
			Java8Parser.PrimaryNoNewArray_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPrimaryNoNewArray_lf_primary_lf_arrayAccess_lf_primary(
			Java8Parser.PrimaryNoNewArray_lf_primary_lf_arrayAccess_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPrimaryNoNewArray_lf_primary_lf_arrayAccess_lf_primary(
			Java8Parser.PrimaryNoNewArray_lf_primary_lf_arrayAccess_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPrimaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primary(
			Java8Parser.PrimaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPrimaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primary(
			Java8Parser.PrimaryNoNewArray_lf_primary_lfno_arrayAccess_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPrimaryNoNewArray_lfno_primary(
			Java8Parser.PrimaryNoNewArray_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPrimaryNoNewArray_lfno_primary(
			Java8Parser.PrimaryNoNewArray_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPrimaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primary(
			Java8Parser.PrimaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPrimaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primary(
			Java8Parser.PrimaryNoNewArray_lfno_primary_lf_arrayAccess_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPrimaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primary(
			Java8Parser.PrimaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPrimaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primary(
			Java8Parser.PrimaryNoNewArray_lfno_primary_lfno_arrayAccess_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassInstanceCreationExpression(
			Java8Parser.ClassInstanceCreationExpressionContext ctx) {
		// System.out.println("Class instance statement "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassInstanceCreationExpression(
			Java8Parser.ClassInstanceCreationExpressionContext ctx) {

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassInstanceCreationExpression_lf_primary(
			Java8Parser.ClassInstanceCreationExpression_lf_primaryContext ctx) {
		// System.out.println("Class instance statement "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassInstanceCreationExpression_lf_primary(
			Java8Parser.ClassInstanceCreationExpression_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterClassInstanceCreationExpression_lfno_primary(
			Java8Parser.ClassInstanceCreationExpression_lfno_primaryContext ctx) {
		// System.out.println("Class instance statement "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitClassInstanceCreationExpression_lfno_primary(
			Java8Parser.ClassInstanceCreationExpression_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterTypeArgumentsOrDiamond(
			Java8Parser.TypeArgumentsOrDiamondContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitTypeArgumentsOrDiamond(
			Java8Parser.TypeArgumentsOrDiamondContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFieldAccess(Java8Parser.FieldAccessContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFieldAccess(Java8Parser.FieldAccessContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFieldAccess_lf_primary(
			Java8Parser.FieldAccess_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFieldAccess_lf_primary(
			Java8Parser.FieldAccess_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterFieldAccess_lfno_primary(
			Java8Parser.FieldAccess_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitFieldAccess_lfno_primary(
			Java8Parser.FieldAccess_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterArrayAccess(Java8Parser.ArrayAccessContext ctx) {
		if (logger) {
			System.out.println("Array Access " + ctx.getText());
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitArrayAccess(Java8Parser.ArrayAccessContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterArrayAccess_lf_primary(
			Java8Parser.ArrayAccess_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitArrayAccess_lf_primary(
			Java8Parser.ArrayAccess_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterArrayAccess_lfno_primary(
			Java8Parser.ArrayAccess_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitArrayAccess_lfno_primary(
			Java8Parser.ArrayAccess_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodInvocation(Java8Parser.MethodInvocationContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodInvocation_lf_primary(
			Java8Parser.MethodInvocation_lf_primaryContext ctx) {
		// System.out.println("Method invocation statement "+ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodInvocation_lf_primary(
			Java8Parser.MethodInvocation_lf_primaryContext ctx) {
		// System.out.println("Method invocation statement "+ctx.getText());

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodInvocation_lfno_primary(
			Java8Parser.MethodInvocation_lfno_primaryContext ctx) {
		// System.out.println("Method invocation statement "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodInvocation_lfno_primary(
			Java8Parser.MethodInvocation_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterArgumentList(Java8Parser.ArgumentListContext ctx) {
		// System.out.println("Enter argument statement "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitArgumentList(Java8Parser.ArgumentListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodReference(Java8Parser.MethodReferenceContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodReference(Java8Parser.MethodReferenceContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodReference_lf_primary(
			Java8Parser.MethodReference_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodReference_lf_primary(
			Java8Parser.MethodReference_lf_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMethodReference_lfno_primary(
			Java8Parser.MethodReference_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMethodReference_lfno_primary(
			Java8Parser.MethodReference_lfno_primaryContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterArrayCreationExpression(
			Java8Parser.ArrayCreationExpressionContext ctx) {
		if (logger) {
			System.out.println("Parent " + ctx.getParent().getText());
			System.out.println("ARRAY CREATION " + ctx.getText());
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitArrayCreationExpression(
			Java8Parser.ArrayCreationExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDimExprs(Java8Parser.DimExprsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDimExprs(Java8Parser.DimExprsContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterDimExpr(Java8Parser.DimExprContext ctx) {
		// System.out.println("Dim statement "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitDimExpr(Java8Parser.DimExprContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterConstantExpression(
			Java8Parser.ConstantExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitConstantExpression(Java8Parser.ConstantExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterExpression(Java8Parser.ExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitExpression(Java8Parser.ExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLambdaExpression(Java8Parser.LambdaExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLambdaExpression(Java8Parser.LambdaExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLambdaParameters(Java8Parser.LambdaParametersContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLambdaParameters(Java8Parser.LambdaParametersContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInferredFormalParameterList(
			Java8Parser.InferredFormalParameterListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInferredFormalParameterList(
			Java8Parser.InferredFormalParameterListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLambdaBody(Java8Parser.LambdaBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLambdaBody(Java8Parser.LambdaBodyContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAssignmentExpression(
			Java8Parser.AssignmentExpressionContext ctx) {

		// System.out.println("Line number "+"2750"+"AssignmentExpression "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAssignmentExpression(
			Java8Parser.AssignmentExpressionContext ctx) {
	}

	/**
	 * {@inheritD
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAssignment(Java8Parser.AssignmentContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterLeftHandSide(Java8Parser.LeftHandSideContext ctx) {
		// System.out.println("Line number "+"2772 "+"LeftHandSide expression "+ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitLeftHandSide(Java8Parser.LeftHandSideContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAssignmentOperator(
			Java8Parser.AssignmentOperatorContext ctx) {
		// System.out.println("Line number "+"2793"+" Assignment Operatot "+ctx.getText()+"Tokens "+ctx.getStart().getText()+" next "+ctx.getStop().getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAssignmentOperator(Java8Parser.AssignmentOperatorContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterConditionalExpression(
			Java8Parser.ConditionalExpressionContext ctx) {

	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitConditionalExpression(
			Java8Parser.ConditionalExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterConditionalOrExpression(
			Java8Parser.ConditionalOrExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitConditionalOrExpression(
			Java8Parser.ConditionalOrExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterConditionalAndExpression(
			Java8Parser.ConditionalAndExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitConditionalAndExpression(
			Java8Parser.ConditionalAndExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterInclusiveOrExpression(
			Java8Parser.InclusiveOrExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitInclusiveOrExpression(
			Java8Parser.InclusiveOrExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterExclusiveOrExpression(
			Java8Parser.ExclusiveOrExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitExclusiveOrExpression(
			Java8Parser.ExclusiveOrExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAndExpression(Java8Parser.AndExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAndExpression(Java8Parser.AndExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEqualityExpression(
			Java8Parser.EqualityExpressionContext ctx) {
		/*
		 * if(!assignment){
		 * System.out.println("Line number "+"2880"+"Equality expression "
		 * +ctx.getText()+" start "+ctx.getStart().getText()); equality=true; }
		 */
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEqualityExpression(Java8Parser.EqualityExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitRelationalExpression(
			Java8Parser.RelationalExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterShiftExpression(Java8Parser.ShiftExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitShiftExpression(Java8Parser.ShiftExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterAdditiveExpression(
			Java8Parser.AdditiveExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitAdditiveExpression(Java8Parser.AdditiveExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterMultiplicativeExpression(
			Java8Parser.MultiplicativeExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitMultiplicativeExpression(
			Java8Parser.MultiplicativeExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnaryExpression(Java8Parser.UnaryExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnaryExpression(Java8Parser.UnaryExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPreIncrementExpression(
			Java8Parser.PreIncrementExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPreDecrementExpression(
			Java8Parser.PreDecrementExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterUnaryExpressionNotPlusMinus(
			Java8Parser.UnaryExpressionNotPlusMinusContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitUnaryExpressionNotPlusMinus(
			Java8Parser.UnaryExpressionNotPlusMinusContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPostfixExpression(Java8Parser.PostfixExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPostfixExpression(Java8Parser.PostfixExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPostIncrementExpression(
			Java8Parser.PostIncrementExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPostIncrementExpression_lf_postfixExpression(
			Java8Parser.PostIncrementExpression_lf_postfixExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPostIncrementExpression_lf_postfixExpression(
			Java8Parser.PostIncrementExpression_lf_postfixExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPostDecrementExpression(
			Java8Parser.PostDecrementExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterPostDecrementExpression_lf_postfixExpression(
			Java8Parser.PostDecrementExpression_lf_postfixExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitPostDecrementExpression_lf_postfixExpression(
			Java8Parser.PostDecrementExpression_lf_postfixExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterCastExpression(Java8Parser.CastExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitCastExpression(Java8Parser.CastExpressionContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void exitEveryRule(ParserRuleContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void visitTerminal(TerminalNode node) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * The default implementation does nothing.
	 * </p>
	 */
	@Override
	public void visitErrorNode(ErrorNode node) {
	}
}