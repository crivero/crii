/*
 [The "BSD license"]
  Copyright (c) 2013 Terence Parr
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. The name of the author may not be used to endorse or promote products
     derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 sowgandh change 
 */
/*
 * changes @tobinp
 */
package com.lspf.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.atn.LexerATNSimulator;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
//import org.graphstream.graph.Graph;
//import org.graphstream.graph.Node;
//import org.graphstream.graph.implementations.SingleGraph;
//import org.graphstream.ui.spriteManager.Sprite;
//import org.graphstream.ui.spriteManager.SpriteManager;

/* This more or less duplicates the functionality of grun (TestRig) but it
 * has a few specific options for benchmarking like -x2 and -threaded.
 * It also allows directory names as commandline arguments. The simplest test is
 * for the current directory:

 ~/antlr/code/grammars-v4/java $ java Test .
 /Users/parrt/antlr/code/grammars-v4/java8/JavaBaseListener.java
 /Users/parrt/antlr/code/grammars-v4/java8/Java8Lexer.java
 /Users/parrt/antlr/code/grammars-v4/java8/JavaListener.java
 /Users/parrt/antlr/code/grammars-v4/java8/JavaParser.java
 /Users/parrt/antlr/code/grammars-v4/java8/Test.java
 Total lexer+parser time 1867ms.
 */
@SuppressWarnings("serial")
public class Test implements Serializable {
	String actual_string = "";
	String code_type = "";

	private boolean logger = false;

	public ArrayList<Vertex> declList = null;
	public ArrayList<Vertex> assignList = null;
	public ArrayList<Vertex> controlList = null;
	public ArrayList<Vertex> returnList = null;
	public ArrayList<Edge> edgeList = null;
	public ArrayList<Vertex> exprList = null;
	public ArrayList<Vertex> incList = null;
	public ArrayList<Vertex> callsiteList = null;
	public HashMap<String, Vertex> lastAssignment = new HashMap<String, Vertex>();
	public HashMap<String, String> type = new HashMap<String, String>();
	public HashMap<String, HashMap<String, Vertex>> lastAssignmentVertex = new HashMap<String, HashMap<String, Vertex>>();
	public HashMap<String, Vertex> functionVertex = new HashMap<String, Vertex>();

	public String filename = "";
	public boolean profile = false;
	public boolean notree = false;
	public boolean gui = false;
	public boolean printTree = false;
	public boolean SLL = false;
	public boolean diag = false;
	public boolean bail = false;
	public boolean x2 = false;
	public boolean threaded = false;
	public boolean quiet = false;
	public Worker[] workers = new Worker[3];
	int windex = 0;

	public CyclicBarrier barrier;

	public volatile boolean firstPassDone = false;

	public Test() {

	}

	private ArrayList<Vertex> cloneList(ArrayList<Vertex> list) {
		ArrayList<Vertex> clone = new ArrayList<Vertex>(list.size());
		for (Vertex currentVertex : list)
			clone.add(new Vertex(currentVertex));
		return clone;
	}

	private ArrayList<Edge> cloneEdgeList(ArrayList<Edge> list) {
		ArrayList<Edge> clone = new ArrayList<Edge>(list.size());
		for (Edge currentEdge : list)
			clone.add(new Edge(currentEdge));
		return clone;
	}

	private HashMap<String, Vertex> cloneMap(HashMap<String, Vertex> map) {
		HashMap<String, Vertex> clone = new HashMap<String, Vertex>();
		Set<String> keys = map.keySet();
		for (String key : keys) {
			clone.put(key, map.get(key));
		}
		return clone;
	}

	private HashMap<String, String> cloneMapType(
			HashMap<String, String> map) {
		HashMap<String, String> clone = new HashMap<String, String>();
		Set<String> keys = map.keySet();
		for (String key : keys) {
			clone.put(key, map.get(key));
		}
		return clone;
	}

	private HashMap<String, HashMap<String, Vertex>> cloneMapAssignment(
			HashMap<String, HashMap<String, Vertex>> map) {
		HashMap<String, HashMap<String, Vertex>> clone = new HashMap<String, HashMap<String, Vertex>>();
		Set<String> keys = map.keySet();
		for (String key : keys) {
			clone.put(key, map.get(key));
		}
		return clone;
	}

	public Test(Test _f1) {
		declList = cloneList(_f1.declList);
		assignList = cloneList(_f1.assignList);
		controlList = cloneList(_f1.controlList);
		returnList = cloneList(_f1.returnList);
		edgeList = cloneEdgeList(_f1.edgeList);
		exprList = cloneList(_f1.exprList);
		incList = cloneList(_f1.incList);
		lastAssignment = cloneMap(_f1.lastAssignment);
		type = cloneMapType(_f1.type);
		lastAssignmentVertex = cloneMapAssignment(_f1.lastAssignmentVertex);
		functionVertex = cloneMap(_f1.functionVertex);
	}

	public class Worker implements Runnable {
		public long parserStart;
		public long parserStop;
		List<String> files;

		public Worker(List<String> files) {
			this.files = files;
		}

		@Override
		public void run() {
			parserStart = System.currentTimeMillis();
			for (String f : files) {
				parseFile(f);
			}
			parserStop = System.currentTimeMillis();
			try {
				barrier.await();
			} catch (InterruptedException ex) {
				return;
			} catch (BrokenBarrierException ex) {
				return;
			}
		}
	}

	public void doAll(String[] fileNameAndParams, String path) {
		List<String> inputFiles = new ArrayList<String>();
		try {
			if (fileNameAndParams.length > 0) {
				for (int i = 0; i < fileNameAndParams.length; i++) {
					if (fileNameAndParams[i].charAt(0) != '-') {
						inputFiles.add(fileNameAndParams[i]);
					}

					if (fileNameAndParams[i].equals("-notree"))
						notree = true;
					else if (fileNameAndParams[i].equals("-gui"))
						gui = true;
					else if (fileNameAndParams[i].equals("-ptree"))
						printTree = true;
					else if (fileNameAndParams[i].equals("-SLL"))
						SLL = true;
					else if (fileNameAndParams[i].equals("-bail"))
						bail = true;
					else if (fileNameAndParams[i].equals("-diag"))
						diag = true;
					else if (fileNameAndParams[i].equals("-2x"))
						x2 = true;
					else if (fileNameAndParams[i].equals("-threaded"))
						threaded = true;
					else if (fileNameAndParams[i].equals("-quiet"))
						quiet = true;
				}
				List<String> javaFiles = new ArrayList<String>();
				for (String fileName : inputFiles) {
					List<String> files = getFilenames(new File(path + "\\"
							+ fileName));
					javaFiles.addAll(files);
				}
				doFiles(javaFiles);

				if (x2) {
					System.gc();
					if (logger) {
						System.out.println("waiting for 1st pass");
					}
					if (threaded)
						while (!firstPassDone) {
						} // spin
					if (logger) {
						System.out.println("2nd pass");
					}
					doFiles(javaFiles);
				}
			} else {
				System.err.println("Usage: java Main <directory or file name>");
			}
		} catch (Exception e) {
			System.err.println("exception: " + e);
			e.printStackTrace(System.err); // so we can get stack trace
		}
		System.gc();
	}

	public void doFiles(List<String> files) throws Exception {
		long parserStart = System.currentTimeMillis();
		// lexerTime = 0;
		if (threaded) {
			barrier = new CyclicBarrier(3, new Runnable() {
				public void run() {
					report();
					firstPassDone = true;
				}
			});
			int chunkSize = files.size() / 3; // 10/3 = 3
			int p1 = chunkSize; // 0..3
			int p2 = 2 * chunkSize; // 4..6, then 7..10
			workers[0] = new Worker(files.subList(0, p1 + 1));
			workers[1] = new Worker(files.subList(p1 + 1, p2 + 1));
			workers[2] = new Worker(files.subList(p2 + 1, files.size()));
			new Thread(workers[0], "worker-" + windex++).start();
			new Thread(workers[1], "worker-" + windex++).start();
			new Thread(workers[2], "worker-" + windex++).start();
		} else {
			for (String f : files) {
				parseFile(f);
			}
			long parserStop = System.currentTimeMillis();
			if (logger) {
				System.out.println("Total lexer+parser time "
						+ (parserStop - parserStart) + "ms.");
			}
		}
	}

	private void report() {
		long time = 0;
		if (workers != null) {
			// compute max as it's overlapped time
			for (Worker w : workers) {
				long wtime = w.parserStop - w.parserStart;
				time = Math.max(time, wtime);
				if (logger) {
					System.out.println("worker time " + wtime + "ms.");
				}
			}
		}
		if (logger) {
			System.out.println("Total lexer+parser time " + time + "ms.");

			System.out.println("finished parsing OK");
			System.out.println(LexerATNSimulator.match_calls
					+ " lexer match calls");

		}
	}

	public List<String> getFilenames(File f) throws Exception {
		List<String> files = new ArrayList<String>();
		getFilenames_(f, files);
		return files;
	}

	public void getFilenames_(File f, List<String> files) throws Exception {
		// If this is a directory, walk each file/dir in that directory
		if (f.isDirectory()) {
			String flist[] = f.list();
			for (int i = 0; i < flist.length; i++) {
				getFilenames_(new File(f, flist[i]), files);
			}
		}

		// otherwise, if this is a java file, parse it!
		else if (((f.getName().length() > 5) && f.getName()
				.substring(f.getName().length() - 5).equals(".java"))) {
			if (logger) {
				System.out.println(f.getAbsolutePath());
			}
			files.add(f.getAbsolutePath());
		}
	}

	private void removeDuplicates(PDGraph graph) {
		Test f1 = graph.getTest();
		if (logger) {
			System.out.println(f1.edgeList);
		}
		Set<Vertex> hsAssign = new HashSet<>();
		hsAssign.addAll(f1.assignList);
		f1.assignList.clear();
		f1.assignList.addAll(hsAssign);
		hsAssign.clear();
		HashSet<String> hs = new HashSet<String>();
		Iterator<Edge> edgeIterator = f1.edgeList.iterator();
		while (edgeIterator.hasNext()) {
			Edge currentEdge = edgeIterator.next();
			if (hs.contains(currentEdge.toString())) {
				edgeIterator.remove();
				continue;
			} else {
				hs.add(currentEdge.toString());
			}
			String temp[] = currentEdge.toString().split("@@");
			if (temp[0].trim().equalsIgnoreCase(
					temp[1].trim().substring(0,
							temp[1].trim().lastIndexOf(")") + 1))) {
				edgeIterator.remove();
			}
		}
		if (logger) {
			System.out.println(f1.edgeList);
		}
	}

	public void parseFile(String f) {
		try {
			if (!quiet)
				System.err.println(f);

			Java8BaseListener.declList.clear();
			Java8BaseListener.assignList.clear();
			Java8BaseListener.callSiteList.clear();
			Java8BaseListener.controlList.clear();
			Java8BaseListener.edgeList.clear();
			Java8BaseListener.incList.clear();
			Java8BaseListener.exprList.clear();
			Java8BaseListener.returnList.clear();
			Java8BaseListener.lastAssignment.clear();
			Java8BaseListener.lastAssignmentVertex.clear();

			Lexer lexer = new Java8Lexer(new ANTLRFileStream(f));

			CommonTokenStream tokens = new CommonTokenStream(lexer);
			Java8Parser parser = new Java8Parser(tokens);

			ParserRuleContext t = parser.compilationUnit();
			ParseTreeWalker walker = new ParseTreeWalker();
			Java8BaseListener listener = new Java8BaseListener();
			walker.walk(listener, t);
			if (printTree)
				System.out.println(t.toStringTree(parser));
			if (notree)
				parser.setBuildParseTree(false);

			if (logger) {
				System.out.println("Total number of edges "
						+ Java8BaseListener.edgeList.size());
			}

			declList = Java8BaseListener.declList;
			returnList = Java8BaseListener.returnList;
			assignList = Java8BaseListener.assignList;
			controlList = Java8BaseListener.controlList;
			edgeList = Java8BaseListener.edgeList;
			incList = Java8BaseListener.incList;
			exprList = Java8BaseListener.exprList;
			callsiteList = Java8BaseListener.callSiteList;
			lastAssignment = Java8BaseListener.lastAssignment;
			type = Java8BaseListener.type;
			lastAssignmentVertex = Java8BaseListener.lastAssignmentVertex;
			functionVertex = Java8BaseListener.functionVertex;
			if (logger) {
				System.out.println("declList size " + declList.size());
				System.out.println("returnList size " + returnList.size());
				System.out.println("assignList size " + assignList.size());
				System.out.println("controlList size " + controlList.size());
				System.out.println("edgeList size " + edgeList.size());
				System.out.println("incList size " + incList.size());
				System.out.println("exprList size " + exprList.size());
				System.out.print("callsiteList " + callsiteList.size());

			}
		} catch (Exception e) {
			System.err.println("parser exception: " + e);
			e.printStackTrace(); // so we can get stack trace
		}
	}

	private void processPattern(Test f, String functionName, String cwd)
			throws CloneNotSupportedException {
		// PDGraph pattern = new PDGraph(
		// loadDataStructuresFromSerFile(functionName));
		PDGraph pattern = new PDGraph(f);
		removeDuplicates(pattern);
		//createGraphStream(pattern, functionName, cwd);
		String[] params = { "1", "2" };
		String[] functions = { "get(s,x)" };
		PDGraph pattern1 = pattern.setPatternParams(params, functions);
		//createGraphStream(pattern1, functionName, cwd);
		String[] params2 = { "2", "2" };
		PDGraph pattern2 = pattern.setPatternParams(params2, functions);
		//createGraphStream(pattern2, functionName, cwd);
	}

	private HashMap<String, Integer> vertexId = new HashMap<String, Integer>();
	private int vertexID = 0;

	private StringBuilder addNodesJSON(ArrayList<Vertex> list,
			StringBuilder jSON) {
		for (Vertex currentVertex : list) {
			String type = currentVertex.getType();
			type = type.replaceAll("%", "");
			String label = currentVertex.getLabel();
			if (!(vertexId.containsKey(label))) {
				vertexID++;
				vertexId.put(label, vertexID);
				jSON.append("{'data': { 'id': " + vertexId.get(label)
						+ ", 'type': '" + type + "', 'label': '"
						+ vertexId.get(label) + ", " + type + "', 'code': '"
						+ label + "' }},");
			}
		}
		return jSON;
	}

	private StringBuilder addEdgesJSON(ArrayList<Edge> list,
			StringBuilder jSON) {
		for (Edge currentEdge : list) {
			Vertex fromVertex = currentEdge.fromVertex;
			Vertex toVertex = currentEdge.toVertex;
			String fromLabel = fromVertex.getLabel();
			String toLabel = toVertex.getLabel();
			if ((vertexId.containsKey(fromLabel) && vertexId
					.containsKey(toLabel))) {
				int source = vertexId.get(fromLabel);
				int target = vertexId.get(toLabel);
				jSON.append("{'data': { 'id': " + Integer.toString(source)
						+ Integer.toString(target) + ", 'source': "
						+ Integer.toString(source) + ", 'target': "
						+ Integer.toString(target) + "},'classes': '"
						+ currentEdge.getType() + "'},");
			}
		}
		return jSON;
	}

	private String getJSONString(PDGraph code) {
		Test f1 = code.getTest();
		StringBuilder jSON = new StringBuilder("[");
		jSON = addNodesJSON(f1.declList, jSON);
		jSON = addNodesJSON(f1.controlList, jSON);
		jSON = addNodesJSON(f1.assignList, jSON);
		jSON = addNodesJSON(f1.returnList, jSON);
		jSON = addNodesJSON(f1.exprList, jSON);
		jSON = addEdgesJSON(f1.edgeList, jSON);
		jSON.deleteCharAt(jSON.length() - 1);
		jSON.append("]");
		return jSON.toString();
	}

	private String processCode(Test f1, String cwd) {
		PDGraph code = new PDGraph(f1);
		removeDuplicates(code);
		// writeToFile(code, functionName);
		// createGraphStream(code, functionName, cwd);
		String jSON = getJSONString(code);
		System.out.println(jSON);
		return jSON;
	}

//	private static Graph createEdge(String fromNode, String fromNodeType,
//			String toNode, String toNodeType, String type, Graph programFlow) {
//		String finalFromNode = fromNodeType + ", " + fromNode;
//		String finalToNode = toNodeType + ", " + toNode;
//		if (logger) {
//			System.out.println("finalFrom:" + finalFromNode);
//			System.out.println("finalTo:" + finalToNode);
//		}
//		programFlow.addEdge(type + ", " + finalFromNode + ", " + finalToNode,
//				finalFromNode, finalToNode);
//		return programFlow;
//	}

//	private static Graph createNodes(String type, ArrayList<Vertex> list,
//			Graph programFlow) {
//		if (type.contains("expr")) {
//			for (int i = 0; i < list.size(); i++) {
//				String current = list.get(i).toString().trim();
//				if (!(current.contains("Sorted") || current.contains("obj"))) {
//					programFlow.addNode(type + current);
//
//				}
//			}
//		} else if (type.contains("control")) {
//			for (int i = 0; i < list.size(); i++) {
//				String current = list
//						.get(i)
//						.toString()
//						.trim()
//						.substring(
//								list.get(i).toString().trim().indexOf(" ") + 1);
//				programFlow.addNode(type + current);
//			}
//		} else {
//			for (int i = 0; i < list.size(); i++) {
//				programFlow.addNode(type + list.get(i).toString().trim());
//			}
//		}
//		return programFlow;
//	}

//	private void createGraphStream(PDGraph graph, String functionName,
//			String cwd) {
//		Test f1 = graph.getTest();
//		Graph programFlow = new SingleGraph("pFlow");
//		programFlow = createNodes("decl, ", f1.declList, programFlow);
//		programFlow = createNodes("inc/dec, ", f1.incList, programFlow);
//		programFlow = createNodes("expr, ", f1.exprList, programFlow);
//		programFlow = createNodes("assign, ", f1.assignList, programFlow);
//		programFlow = createNodes("control, ", f1.controlList, programFlow);
//		for (int i = 0; i < f1.edgeList.size(); i++) {
//			Edge currentEdge = f1.edgeList.get(i);
//			String[] currentEdgeString = currentEdge.toString().split("@@");
//			currentEdgeString[0] = currentEdgeString[0].trim();
//			int fromIndex = 0;
//			int count = currentEdgeString[0].length()
//					- currentEdgeString[0].replace("%", "").length();
//			if (count > 2) {
//				fromIndex = currentEdgeString[0].indexOf("%");
//				fromIndex++;
//			}
//			currentEdgeString[0] = currentEdgeString[0].substring(
//					currentEdgeString[0].indexOf('%', fromIndex) + 1,
//					currentEdgeString[0].lastIndexOf('%'));
//			currentEdgeString[1] = currentEdgeString[1].trim();
//			fromIndex = 0;
//			count = currentEdgeString[1].length()
//					- currentEdgeString[1].replace("%", "").length();
//			if (count > 2) {
//				fromIndex = currentEdgeString[1].indexOf("%");
//				fromIndex++;
//			}
//			currentEdgeString[1] = currentEdgeString[1].substring(
//					currentEdgeString[1].indexOf('%', fromIndex) + 1,
//					currentEdgeString[1].lastIndexOf('%'));
//			String fromNode = currentEdge.getFromVertex().toString();
//			String fromNodeType = currentEdgeString[0];
//			String temp[] = fromNode.split(" ");
//			if (temp.length == 2) {
//				fromNode = temp[1];
//			}
//			fromNodeType = fromNodeType.replaceAll("%", "");
//			String toNode = currentEdge.getToVertex().toString();
//			String toNodeType = currentEdgeString[1];
//			String temp1[] = toNode.split(" ");
//			if (temp1.length == 2) {
//				toNode = temp1[1];
//			}
//			toNodeType = toNodeType.replaceAll("%", "");
//			String type = currentEdge.getType();
//			createEdge(fromNode, fromNodeType, toNode, toNodeType, type,
//					programFlow);
//		}
//		programFlow = beautifyGraph(programFlow);
//		programFlow.display();
//	}

	private void writeToFile(PDGraph graph, String functionName) {
		String cwd = System.getProperty("user.dir");
		if (cwd.contains("\\bin")) {
			String[] split = cwd.split("\\\\");
			StringBuilder tempcwd = new StringBuilder("");
			for (int i = 0; i < split.length - 1; i++) {
				tempcwd.append(split[i]);
				if (i != split.length - 1)
					tempcwd.append("\\");
			}
			cwd = tempcwd.toString();
		}
		Test f1 = graph.getTest();
		Map<String, String> sourceTarget = new HashMap<String, String>();
		for (int i = 0; i < f1.edgeList.size(); i++) {
			Edge currentEdge = f1.edgeList.get(i);
			String[] currentEdgeString = currentEdge.toString().split("@@");
			currentEdgeString[0] = currentEdgeString[0].trim();
			int fromIndex = 0;
			int count = currentEdgeString[0].length()
					- currentEdgeString[0].replace("%", "").length();
			if (count > 2) {
				fromIndex = currentEdgeString[0].indexOf("%");
				fromIndex++;
			}
			currentEdgeString[0] = currentEdgeString[0].substring(
					currentEdgeString[0].indexOf('%', fromIndex) + 1,
					currentEdgeString[0].lastIndexOf('%'));
			currentEdgeString[1] = currentEdgeString[1].trim();
			fromIndex = 0;
			count = currentEdgeString[1].length()
					- currentEdgeString[1].replace("%", "").length();
			if (count > 2) {
				fromIndex = currentEdgeString[1].indexOf("%");
				fromIndex++;
			}
			currentEdgeString[1] = currentEdgeString[1].substring(
					currentEdgeString[1].indexOf('%', fromIndex) + 1,
					currentEdgeString[1].lastIndexOf('%'));
			String fromNode = currentEdge.getFromVertex().toString();
			String fromNodeType = currentEdgeString[0];
			String temp[] = fromNode.split(" ");
			if (temp.length == 2) {
				fromNode = temp[1];
			}
			fromNode = fromNode.trim();
			fromNodeType = fromNodeType.replaceAll("%", "");
			String toNode = currentEdge.getToVertex().toString();
			String toNodeType = currentEdgeString[1];
			String temp1[] = toNode.split(" ");
			if (temp1.length == 2) {
				toNode = temp1[1];
			}
			toNodeType = toNodeType.replaceAll("%", "");
			toNode = toNode.trim();
			// String type = currentEdge.getType();
			if (fromNodeType.contains("expr") || toNodeType.contains("expr")) {
				continue;
			}
			if (logger) {
				System.out.println("fromNode:" + fromNode + "-->toNode:"
						+ toNode);
			}
			if (sourceTarget.containsKey(fromNodeType + ", " + fromNode)) {
				String targets = sourceTarget.get(fromNodeType + ", "
						+ fromNode);
				updateMap(fromNodeType + ", " + fromNode);
				int currentTarget = getMapValue(toNodeType + ", " + toNode);
				int ct = targets.length() - targets.replace(",", "").length();
				if (ct == 0
						&& targets.equalsIgnoreCase(Integer
								.toString(currentTarget))) {
					continue;
				}
				targets = targets + "," + Integer.toString(currentTarget);
				sourceTarget.put(fromNodeType + ", " + fromNode, targets);
			} else {
				int currentTarget = getMapValue(toNodeType + ", " + toNode);
				updateMap(fromNodeType + ", " + fromNode);
				sourceTarget.put(fromNodeType + ", " + fromNode,
						Integer.toString(currentTarget));
			}
		}
		if (logger) {
			System.out.println("ORDER");
			System.out.println(order);
		}
		StringBuilder content = new StringBuilder("name\ttarget");
		for (String key : order) {
			content.append("\n");
			String target = sourceTarget.get(key);
			if (target == null) {
				target = "";
			}
			content.append(key + "\t" + target);
		}

		try {
			File file = new File(cwd + "\\Test\\" + functionName + "data.tsv");
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			String finalContent = content.toString();
			bw.write(finalContent);
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private int mapCounter = 0;
	private Map<String, Integer> map = new HashMap<String, Integer>();
	protected ArrayList<String> order = new ArrayList<String>();

	private int getMapValue(String key) {
		if (map.containsKey(key)) {
			return map.get(key);
		} else {
			order.add(key);
			map.put(key, mapCounter);
			mapCounter = mapCounter + 1;
			return map.get(key);
		}
	}

	private void updateMap(String key) {
		if (!map.containsKey(key)) {
			order.add(key);
			map.put(key, mapCounter);
			mapCounter = mapCounter + 1;
		}
	}

//	private Graph beautifyGraph(Graph programFlow) {
//		for (Node node : programFlow) {
//			SpriteManager sman = new SpriteManager(programFlow);
//			String nodeId = node.getId();
//			node.addAttribute("ui.label", nodeId);
//			Sprite s = sman.addSprite("XXX");
//			s.addAttribute("XXXXX");
//			s.attachToNode(node.getId());
//			if (nodeId.contains("decl")) {
//				node.addAttribute("ui.style", "fill-color: rgb(0,100,255);");
//			} else if (nodeId.contains("control")) {
//				node.addAttribute("ui.style", "fill-color: rgb(176, 23, 31);");
//			} else if (nodeId.contains("assign")) {
//				node.addAttribute("ui.style", "fill-color: rgb(0, 255, 127);");
//			} else if (nodeId.contains("inc/dec")) {
//				node.addAttribute("ui.style", "fill-color: rgb(255, 255, 0);");
//			} else if (nodeId.contains("expr")) {
//				node.addAttribute("ui.style", "fill-color: rgb(238, 18, 137);");
//			}
//		}
//		return programFlow;
//	}

	private String makeJavaFile(String body, String functionName,
			String cwd) throws IOException {
		cwd = cwd + "\\Resources";
		File theDir = new File(cwd);
		if (!theDir.exists()) {
		    try{
		        theDir.mkdir();
		    } 
		    catch(SecurityException se){
		        //handle it
		    	se.printStackTrace();
		    }        
		}
		String fileName = "";
		String arguments = "()";
		if (functionName.contains("1")) {
			fileName = "ProblemOne";
			arguments = "(int a)";
		} else {
			fileName = "ProblemTwo";
			arguments = "(String f, String y)";
		}
		Random random = new Random();
		int rd = 0;
		// to make sure the file name is unique for multiple requests.
		while (true) {
			rd = random.nextInt(100000 - 1000 + 1) + 1000;
			if (new File(cwd, fileName + Integer.toString(rd) + ".java")
					.exists()) {
				continue;
			} else {
				break;
			}
		}
		String className = fileName;
		fileName = fileName + Integer.toString(rd) + ".java";
		File currentFile = new File(cwd, fileName);
		if (!currentFile.exists()) {
			currentFile.createNewFile();
		}
		StringBuilder sb = new StringBuilder("public class " + className
				+ "{\n");
		sb.append("public void " + functionName + arguments + "{");
		sb.append(body);
		sb.append("}}");
		FileWriter fw = new FileWriter(currentFile.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(sb.toString());
		bw.close();
		return fileName;
	}

	public Test driver(String functionName, String cwd, String body)
			throws IOException {
		Java8BaseListener.function_name = functionName;
		String fileName = makeJavaFile(body, functionName, cwd);
		String[] fileNamesAndParams = new String[1];
		fileNamesAndParams[0] = fileName;
		Test f = new Test();
		f.doAll(fileNamesAndParams, cwd + "\\Resources");
		File fileToDelete = new File(cwd + "\\Resources", fileName);
		fileToDelete.delete();
		vertexId.clear();
		vertexID = 0;
		return (f);
	}

	public String getGraphJSON(String methodName, String body)
			throws IOException {
		String cwd = System.getProperty("user.dir");
		if (cwd.contains("\\bin")) {
			String[] split = cwd.split("\\\\");
			StringBuilder tempcwd = new StringBuilder("");
			for (int i = 0; i < split.length - 1; i++) {
				tempcwd.append(split[i]);
				if (i != split.length - 1)
					tempcwd.append("\\");
			}
			cwd = tempcwd.toString();
		}
		methodName = "solution";
		body = "String year;String pos;int i = 0;int gold = 0;Scanner s = new Scanner(new File(f));while (s.hasNext()) {if (i % 5 == 2) {year = s.next();}if (i % 5 == 3) {pos = s.next();}if (i % 5 == 4 && year.equals(y) && pos.equals(�1�)) {gold++;}i++;}s.close();System.out.println(�Medals: � + gold);";
		Test f1 = driver(methodName, cwd, body);
		String jSON = processCode(f1, cwd);
		return jSON;
	}

	public static void main(String[] args) throws IOException {
		String cwd = System.getProperty("user.dir");
		if (cwd.contains("\\bin")) {
			String[] split = cwd.split("\\\\");
			StringBuilder tempcwd = new StringBuilder("");
			for (int i = 0; i < split.length - 1; i++) {
				tempcwd.append(split[i]);
				if (i != split.length - 1)
					tempcwd.append("\\");
			}
			cwd = tempcwd.toString();
		}
		//Test t = new Test();
		// Test f = t.driver("patternOne", "Patterns.java", cwd, "");
		// t.processPattern(f, "patternOne", cwd);
//		Test f1 = t.driver(
//				"assignment2",
//				cwd,
//				"String year;String pos;int i = 0;int gold = 0;Scanner s = new Scanner(new File(f));while (s.hasNext()) {if (i % 5 == 2) {year = s.next();}if (i % 5 == 3) {pos = s.next();}if (i % 5 == 4 && year.equals(y) && pos.equals(�1�)) {gold++;}i++;}s.close();System.out.println(�Medals: � + gold);");
//		t.processCode(f1, cwd);
	}
}