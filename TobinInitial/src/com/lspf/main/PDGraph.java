package com.lspf.main;

import java.util.ArrayList;
import java.util.HashMap;

public class PDGraph {
	private Test f1 = null;

	public PDGraph(Test _f1) {
		f1 = new Test(_f1);
	}

	public Test getTest() {
		return f1;
	}

	public PDGraph setPatternParams(String[] params, String[] function)
			throws CloneNotSupportedException {
		Test f2 = new Test(f1);
		f2 = setValues("$param", params, f2);
		String[] replacedfunction = handleFunctions(function, f2);
		f2 = setValues("$function", replacedfunction, f2);
		return (new PDGraph(f2));
	}

	private Test setValues(String type, String[] values, Test f2) {
		for (int i = 0; i < values.length; i++) {
			f2.declList = updateList(i + 1, values[i], f2.declList, type);
			f2.assignList = updateList(i + 1, values[i], f2.assignList, type);
			f2.controlList = updateList(i + 1, values[i], f2.controlList, type);
			f2.returnList = updateList(i + 1, values[i], f2.returnList, type);
			f2.exprList = updateList(i + 1, values[i], f2.exprList, type);
			f2.incList = updateList(i + 1, values[i], f2.incList, type);
			f2.edgeList = updateEdgeList(i + 1, values[i], f2.edgeList, type);
		}
		return f2;
	}

	private String[] handleFunctions(String[] functionValues, Test f2) {
		String[] replacedValues = new String[functionValues.length];
		for (int i = 0; i < functionValues.length; i++) {
			String current = functionValues[i];
			HashMap<String, Vertex> currentLastAssign = f2.lastAssignmentVertex
					.get("$function" + Integer.toString(i + 1));
			Vertex fromVertex = f2.functionVertex.get("$function"
					+ Integer.toString(i + 1));
			if (current.contains("get")) {
				String temp = current.substring(current.indexOf("(") + 1,
						current.indexOf(")"));
				temp = temp.trim();
				String expression = "";
				if (temp.contains(",")) {
					String[] variables = temp.split(",");
					if (f2.type.containsKey(variables[0])) {
						if (f2.type.get(variables[0]).contains(" array")) {
							expression = variables[0] + "[" + variables[1]
									+ "]";
						} else {
							expression = variables[0] + ".get(" + variables[1]
									+ ")";
						}
						addEdges(variables, currentLastAssign, fromVertex, f2);
					}
				} else {
					if (f2.type.get(temp).equalsIgnoreCase("Scanner")) {
						expression = temp + ".next()";
					}
					String[] variable = new String[1];
					variable[0] = temp;
					addEdges(variable, currentLastAssign, fromVertex, f2);
				}
				if (expression != "") {
					replacedValues[i] = expression;
				}
			}
		}
		return replacedValues;
	}

	private void addEdges(String[] variables,
			HashMap<String, Vertex> lastAssign, Vertex fromVertex, Test f2) {
		for (int i = 0; i < variables.length; i++) {
			if (lastAssign.containsKey(variables[i])) {
				Edge newEdge = new Edge(fromVertex,
						lastAssign.get(variables[i]), "data");
				f2.edgeList.add(newEdge);
			}
		}
	}

	private ArrayList<Vertex> updateList(int paramIndex, String paramValue,
			ArrayList<Vertex> list, String type) {
		for (int i = 0; i < list.size(); i++) {
			Vertex current = list.get(i);
			current = changeLabelIfNeeded(current,
					Integer.toString(paramIndex), paramValue, type);
		}
		return list;
	}

	private Vertex changeLabelIfNeeded(Vertex current, String paramIndex,
			String paramValue, String type) {
		if (type == "$function") {
			paramIndex = paramIndex + "()";
		}
		String label = current.getLabel();
		if (label.contains(type + paramIndex)) {
			label = label.replace(type + paramIndex, paramValue);
			current.setLabel(label);
		}
		return current;
	}

	private ArrayList<Edge> updateEdgeList(int paramIndex, String paramValue,
			ArrayList<Edge> list, String type) {
		for (int i = 0; i < list.size(); i++) {
			Edge currentEdge = list.get(i);
			Vertex fromVertex = currentEdge.fromVertex;
			fromVertex = changeLabelIfNeeded(fromVertex,
					Integer.toString(paramIndex), paramValue, type);
			currentEdge.setFromVertex(fromVertex);
			Vertex toVertex = currentEdge.toVertex;
			toVertex = changeLabelIfNeeded(toVertex,
					Integer.toString(paramIndex), paramValue, type);
			currentEdge.setToVertex(toVertex);
		}
		return list;
	}

	private void initDataStructures() {

	}

	public String[] feedbackExact(PDGraph g) {
		initDataStructures();
		String feedBack = "";
		String[] finalFeedBack = feedBack.split(",");
		return finalFeedBack;
	}
}