package com.lspf.main;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Edge implements Serializable {

	public Vertex fromVertex;
	public Vertex toVertex;
	public String type;

	public Edge() {
	}

	public Edge(Vertex fromVertex, Vertex toVertex, String type) {
		this.fromVertex = fromVertex;
		this.toVertex = toVertex;
		this.type = type;
	}

	public Edge(Edge edgeToCopy) {
		this.fromVertex = new Vertex(edgeToCopy.fromVertex);
		this.toVertex = new Vertex(edgeToCopy.toVertex);
		this.type = new String(edgeToCopy.getType());
	}

	@Override
	public String toString() {
		return fromVertex.getLabel() + " (" + fromVertex.getType() + ") "
				+ " @@ " + toVertex.getLabel() + " (" + toVertex.getType()
				+ ") " + " with type " + getType();
	}

	public Vertex getFromVertex() {
		return fromVertex;
	}

	public void setFromVertex(Vertex fromVertex) {
		this.fromVertex = fromVertex;
	}

	public Vertex getToVertex() {
		return toVertex;
	}

	public void setToVertex(Vertex toVertex) {
		this.toVertex = toVertex;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fromVertex == null) ? 0 : fromVertex.hashCode());
		result = prime * result
				+ ((toVertex == null) ? 0 : toVertex.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (fromVertex == null) {
			if (other.fromVertex != null)
				return false;
		} else if (!fromVertex.equals(other.fromVertex))
			return false;
		if (toVertex == null) {
			if (other.toVertex != null)
				return false;
		} else if (!toVertex.equals(other.toVertex))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}
